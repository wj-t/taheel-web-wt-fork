/* eslint-disable */
import { Navigate } from 'react-router-dom';
import DashboardLayout from 'src/Core/Components/DashboardLayout';
import MainLayout from 'src/Core/Components/MainLayout';
import Account from 'src/Modules/Account/Pages/Account';
import CreateTemporaryLicense from 'src/Modules/CenterServices/TemporaryLicense/Pages/CreateTemporaryLicense';
import CreateTempLicenseLandingPage from 'src/Modules/CenterServices/TemporaryLicense/Pages/CreateTempLicenseLandingPage';

import CreatefinalLicense from 'src/Modules/CenterServices/FinalLicense/Pages/CreateFinalLicense';
import CreateFinalLicenseRenewal from 'src/Modules/CenterServices/FinalLicense/Pages/CreateFinalLicenseRenewal';
import Dashboard from 'src/Modules/Dashboard/Pages/Dashboard';
import Login from 'src/Modules/UserAuthentication/Login/Pages/Login';
import NotFound from 'src/Modules/Public/NotFound';
import ServicesList from './Modules/CenterServices/ServicesList/Pages/ServicesList';
import Register from 'src/Modules/UserAuthentication/Registration/Pages/Register';
import Settings from 'src/Modules/Public/Settings/Pages/Settings';
import OTPLogin from 'src/Modules/UserAuthentication/Login/Pages/LoginOtp';
import Home from './Modules/Public/Home';
import About from './Modules/Public/AboutUs';
import Faq from './Modules/Public/faq';
import Services from './Modules/Public/Services';
import ContactUs from './Modules/Public/ContactUs';
import ForgetPassword from './Modules/UserAuthentication/ForgetPassword/Pages/ForgetPassword';
import CentersDetails from './Modules/CenterManagement/Pages/CentersDetails';
import Centers from './Modules/CenterManagement/Pages/Centers';
import AddCommissioner from './Modules/CenterManagement/Pages/AddCommissioner';
import CommissionersManagement from './Modules/CenterManagement/Pages/CommissionersManagement';
import DownloadDoc from './Modules/UserAuthentication/Login/Pages/DownloadDoc';
import { LICENSE_FORM_TYPES, REQUEST_STATUS } from 'src/Core/Utils/enums'
import TransferCenterLocationRequest from 'src/Modules/CenterServices/TransferCenterLocation/Pages/TransferCenterLocationRequest';
import TransferCenterLocationSummary from 'src/Modules/CenterServices/TransferCenterLocation/Pages/TransferCenterLocationSummary';
import CenterList from './Modules/CenterServices/ProgramRegisteration/Pages/CenterList';
import UpdateProgram from './Modules/CenterServices/ProgramRegisteration/Pages/UpdateProgramWizard';
import CenterPrograms from './Modules/CenterServices/ProgramRegisteration/Pages/CenterPrograms';
import TransferNewOwnership from 'src/Modules/CenterServices/TransferCenterOwnership/Pages/TransferNewOwnership';
import TransferCenterOwnership from './Modules/CenterServices/TransferCenterOwnership/Pages/TransferCenterOwnership';
import TransferCenterOwnershipSummary from './Modules/CenterServices/TransferCenterOwnership/Pages/TransferCenterOwnershipSummary';
import ProgramRequestSummary from './Modules/CenterServices/ProgramRegisteration/Pages/ProgamRequestSummary';
import CenterRequests from './Modules/CenterRequests/Pages/CenterRequests';
import CancelInitialApproval from './Modules/CenterServices/CancelInitialApproval/Pages/CancelInitialApproval';
import CancelInitialApprovalSummary from './Modules/CenterServices/CancelInitialApproval/Pages/CancelInitialApprovalSummary';
import FinalLicenseLandingPage from './Modules/CenterServices/FinalLicense/Pages/FinalLicenseLandingPage';

const routes = (isLoggedIn) => [
  {
    path: '/',
    element: <MainLayout />,
    children: [
      { path: 'login', element: isLoggedIn === "" ? <Login /> : <Navigate to="/app/dashboard" /> },
      { path: 'otplogin', element: <OTPLogin /> },
      { path: 'downloadDoc', element: <DownloadDoc /> },
      { path: 'Home', element: <Home /> },
      { path: '/about', element: <About /> },
      { path: '/faq', element: <Faq /> },
      { path: '/services', element: <Services /> },
      { path: '/contactus', element: <ContactUs /> },
      { path: '/forgetpassword', element: <ForgetPassword /> },
      { path: 'register', element: isLoggedIn === "" ? <Register /> : <Navigate to="/app/dashboard" /> },
      { path: '404', element: <NotFound /> },
      {
        path: '/', element: isLoggedIn === "" ? <Navigate to="/home" /> : <Navigate to="/app/dashboard" />,
      },
      { path: '*', element: <Navigate to="/404" /> }
    ]
  },
  {
    path: 'app',
    element: isLoggedIn !== "" ? <DashboardLayout /> : <Navigate to="/login" />,
    children: [
      { path: 'account', element: <Account /> },
      { path: 'centers', element: <Centers /> },
      { path: 'centersDetails', element: <CentersDetails /> },
      { path: 'AddCommissioner', element: <AddCommissioner /> },
      { path: 'CommissionersManagement', element: <CommissionersManagement /> },
      { path: 'dashboard', element: <Dashboard /> },
      { path: 'center-services-list', element: <ServicesList /> },
      { path: 'settings', element: <Settings /> },
      { path: 'center-requests', element: <CenterRequests type={LICENSE_FORM_TYPES.ALL} /> },



      { path: '*', element: <Navigate to="/404" /> }
    ]
  },
  {
    path: 'center-services',
    element: isLoggedIn !== "" ? <DashboardLayout /> : <Navigate to="/login" state={{ prevPath: location.pathname, search: location.search }} />,
    children: [
      { path: 'templicense', element: <CreateTempLicenseLandingPage /> },
      { path: 'templicenseCont', element: <CreateTemporaryLicense /> },

      { path: 'finallicense', element: <FinalLicenseLandingPage /> },
      { path: 'finallicenseCont', element: <CreatefinalLicense /> },
      { path: 'updatefinallicenserenewal', element: <CreatefinalLicense /> },
      { path: 'editfinallicense', element: <CreatefinalLicense /> },
      { path: 'finallicenserenewal', element: <CreateFinalLicenseRenewal /> },
      { path: 'transfercenter', element: <TransferCenterLocationRequest /> },
      { path: 'programRegisteration', element: <CenterList /> },
      { path: 'UpdateProgram', element: <UpdateProgram /> },
      { path: 'ProgramRequestSummary', element: <ProgramRequestSummary/> },

      { path: 'transfercentersummary', element: <TransferCenterLocationSummary /> },
      { path: 'centerPrograms', element: <CenterPrograms /> },
      { path: 'transNewOnership', element: <TransferNewOwnership /> },
      { path: 'transfercenterownership', element: <TransferCenterOwnership /> },
      { path: 'transferCenterOwnershipSummary', element: <TransferCenterOwnershipSummary /> },
      { path: 'cancelInitialApproval', element: <CancelInitialApproval /> },
      { path: 'cancelInitialApprovalSummary', element: <CancelInitialApprovalSummary /> },

      { path: '*', element: <Navigate to="/404" /> }
    ]
  }
];

export default routes;
