import { v4 as uuid } from 'uuid';

export default [
  {
    id: uuid(),
    description: ' تتيح هذه الخدمة  للمركز إصدار موافقة مبدئية لمركز تأهيل أهلي',
    media: '/static/images/products/clock.png',
    title: 'إصدار موافقة مبدئية لمركز تأهيل أهلي',
    url: '/center-services/templicense',
    isActive: true
  },
  {
    id: uuid(),
    description: ' تتيح هذه الخدمة للمركز إصدار ترخيص نهائي لمركز تأهيل أهلي',
    media: '/static/images/products/app.png',
    title: 'إصدار ترخيص',
    url: '/center-services/finallicense',
    isActive: true
  },
  {
    id: uuid(),
    description: 'طلب تجديد رخصة نهائية',
    media: '/static/images/products/renew.png',
    title: 'طلب تجديد رخصة نهائية',
    url: '/center-services/finallicenserenewal',
    isActive: true
  },
  {
    id: uuid(),
    description: 'نقل مقر مركز أهلي',
    media: '/static/images/products/checklist.png',
    title: 'نقل مقر مركز أهلي',
    url: '/center-services/transfercenter',
    isActive: true
  },
  {

    id: uuid(),

    description: 'نقل ملكية مركز أهلي',

    media: '/static/images/products/help_hand.png',

    title: 'نقل ملكية مركز أهلي',

    url: '/center-services/transfercenterownership',

    isActive: true

  },
  {
    id: uuid(),
    description: 'التسجيل في البرامج المعتمدة',
    media: '/static/images/products/app.png',
    title: 'التسجيل في البرامج المعتمدة',
    url: '/center-services/programRegisteration',
    isActive: true
  },
  {
    id: uuid(),
    description: ' إلغاء موافقة مبدئية',
    media: '/static/images/products/checklist.png',
    title: ' إلغاء موافقة مبدئية',
    url: '/center-services/cancelInitialApproval',
    isActive: true
  },
  {
    id: uuid(),
    description: 'تتيح هذه الخدمة للمركز الانضمام لبرنامج تحمل الدولة للرسوم',
    media: '/static/images/products/help_hand.png',
    title: 'برنامج تحمل الدولة للرسوم',
    url: '/center-services/survey',
    isActive: false
  },
  {
    id: uuid(),
    description: ' تتيح هذه الخدمة للمركز إصدار ترخيص نهائي لمركز تأهيل أهلي',
    media: '/static/images/products/app.png',
    title: 'إصدار ترخيص نهائي لمركز تأهيل أهلي',
    url: '/center-services/finallicense',
    isActive: false
  },
  {
    id: uuid(),
    description: ' تتيح هذه الخدمة للمركز إصدار ترخيص مؤقت لمركز تأهيل أهلي',
    media: '/static/images/products/checklist.png',
    title: 'إصدار ترخيص مبدئي لمركز تأهيل أهلي',
    url: '/center-services/survey',
    isActive: false
  },
];
