import { v4 as uuid } from 'uuid';

export default [
  {
    id: uuid(),
    createdAt: '03/04/2019',
    description: 'إضافة أو تحديث سجل الاجتماعي',
    media: '/static/images/products/clock.png',
    title: 'إضافة أو تحديث سجل الاجتماعي',
    totalDownloads: '857',
    url: '/center-services/survey',
    isActive: true
  },
  {
    id: uuid(),
    description: 'التسجيل في مركز أهلي',
    media: '/static/images/products/checklist.png',
    title: 'التسجيل في مركز أهلي',
    url: '/center-services/survey',
    isActive: true
  },
  {
    id: uuid(),
    description: 'تتيح هذه الخدمة للمركز الانضمام لبرنامج تحمل الدولة للرسوم',
    media: '/static/images/products/help_hand.png',
    title: 'التسجيل في برنامج تحمل الدولة للرسوم',
    url: '/center-services/survey',
    isActive: true
  },
  {
    id: uuid(),
    description: 'الانتقال الى مركز أهلي اخر',
    media: '/static/images/products/app.png',
    title: 'الانتقال الى مركز أهلي اخر',
    url: '/center-services/survey',
    isActive: true
  },
  {
    id: uuid(),
    description: 'خدمة طلب تقييم الإعاقة',
    media: '/static/images/products/checklist.png',
    title: 'خدمة طلب تقييم الإعاقة',
    url: '/center-services/survey',
    isActive: true
  },
  {
    id: uuid(),
    description: 'اسناد يتيم لأسرة كافلة',
    media: '/static/images/products/checklist.png',
    title: 'اسناد يتيم لأسرة كافلة',
    url: '/center-services/survey',
    isActive: true
  },
  {
    id: uuid(),
    description: 'طلب استضافة يتيم لدى أسرة صديقة',
    media: '/static/images/products/checklist.png',
    title: 'طلب استضافة يتيم لدى أسرة صديقة',
    url: '/center-services/survey',
    isActive: true
  },
  {
    id: uuid(),
    description: 'خدمة طلب بطاقة التسهيلات المرورية',
    media: '/static/images/products/checklist.png',
    title: 'خدمة طلب بطاقة التسهيلات المرورية',
    url: '/center-services/survey',
    isActive: true
  }
];
