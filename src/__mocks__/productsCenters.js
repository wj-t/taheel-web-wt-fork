import { v4 as uuid } from 'uuid';

export default [
  {
    id: uuid(),
    createdAt: '03/04/2019',
    description: 'خدمة إصدار شهادة ترخيص مؤقت لمركز أهلي ',
    media: '/static/images/products/clock.png',
    title: 'خدمة إصدار شهادة ترخيص مؤقت لمركز أهلي ',
    totalDownloads: '857',
    url: '/center-services/survey',
    isActive: true
  },
  {
    id: uuid(),
    description: 'خدمة تغيير موقع لترخيص مؤقت لمركز أهلي ',
    media: '/static/images/products/checklist.png',
    title: 'خدمة تغيير موقع لترخيص مؤقت لمركز أهلي ',
    url: '/center-services/survey',
    isActive: true
  },
  {
    id: uuid(),
    description: 'خدمة إلغاء شهادة ترخيص مؤقت لمركز أهلي ',
    media: '/static/images/products/help_hand.png',
    title: 'خدمة إلغاء شهادة ترخيص مؤقت لمركز أهلي ',
    url: '/center-services/survey',
    isActive: true
  },
  {
    id: uuid(),
    description: 'خدمة إصدار شهادة ترخيص نهائي لمركز أهلي ',
    media: '/static/images/products/app.png',
    title: 'خدمة إصدار شهادة ترخيص نهائي لمركز أهلي ',
    url: '/center-services/survey',
    isActive: true
  },
  {
    id: uuid(),
    description: 'خدمة تجديد ترخيص نهائي لمركز أهلي',
    media: '/static/images/products/checklist.png',
    title: 'خدمة تجديد ترخيص نهائي لمركز أهلي',
    url: '/center-services/survey',
    isActive: true
  },
  {
    id: uuid(),
    description: 'خدمة نقل ملكية لمركز أهلي ',
    media: '/static/images/products/checklist.png',
    title: 'خدمة نقل ملكية لمركز أهلي ',
    url: '/center-services/survey',
    isActive: true
  },
  {
    id: uuid(),
    description: 'خدمة نقل مقر لمركز أهلي',
    media: '/static/images/products/checklist.png',
    title: 'خدمة نقل مقر لمركز أهلي',
    url: '/center-services/survey',
    isActive: true
  },
  {
    id: uuid(),
    description: 'طلب إلغاء ترخيص لمركز أهلي (من المالك)',
    media: '/static/images/products/checklist.png',
    title: 'طلب إلغاء ترخيص لمركز أهلي (من المالك)',
    url: '/center-services/survey',
    isActive: true
  },
  {
    id: uuid(),
    description: 'التسجيل في برنامج تحمل الدولة للرسوم',
    media: '/static/images/products/checklist.png',
    title: 'التسجيل في برنامج تحمل الدولة للرسوم',
    url: '/center-services/survey',
    isActive: true
  },
  {
    id: uuid(),
    description: 'خدمة نقل ملكية لمركز أهلي ',
    media: '/static/images/products/checklist.png',
    title: 'خدمة نقل ملكية لمركز أهلي ',
    url: '/center-services/survey',
    isActive: false
  },
  {
    id: uuid(),
    description: 'خدمة نقل ملكية لمركز أهلي ',
    media: '/static/images/products/checklist.png',
    title: 'خدمة نقل ملكية لمركز أهلي ',
    url: '/center-services/survey',
    isActive: false
  },
  {
    id: uuid(),
    description: 'خدمة نقل ملكية لمركز أهلي ',
    media: '/static/images/products/checklist.png',
    title: 'خدمة نقل ملكية لمركز أهلي ',
    url: '/center-services/survey',
    isActive: false
  }
];
