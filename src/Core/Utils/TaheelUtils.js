
import moment from 'moment-hijri';
import { isValidElement } from 'react';


const staffTypes = ["معلم تربية خاصة", "أخصائي اجتماعي", "مراقب اجتماعي", "حارس", "عامل تنظيفات", "مشرف فني عام", "اخصائي نفسي و توجيه اجتماعي", "عامل رعاية شخصية", "مدير", "سائق", "مرافق سائق", "أخصائي علاج طبيعي", "أخصائي علاج وظيفي", "أخصائي نطق و تخاطب", "ممرض"]

const dateFormatter = (date, formate = 'iDD/iMM/iYYYY', formateTo = 'iDD iMMM iYYYY') => {
    return moment(date, formate).format(formateTo)
}

const getDocId = (docs) => {
    if (!!docs) {
        if (docs.length > 0) {
            if (!!docs?.Document?.id || !!docs?.Document?.map(d => d?.id)[0] || !!docs?.map(d => d?.Document)[0] || !!docs?.map(d => d?.Document?.id)[0] || !!docs?.id) {
                return [docs?.Document?.id] || docs?.Document?.map(d => d?.id) || docs?.map(d => d?.Document) || docs?.map(d => d?.Document?.id) || [docs?.id]
            } else if (!!docs[0] && docs[0] != '') {
                return [docs]
            } else {
                return null
            }
        } else if (!!docs?.id) {
            return [docs.id]
        } else if (!!docs?.ID) {
            return [docs.ID]
        } else if (docs != '') {
            return [docs]
        }
        else {
            return null
        }
    }
    return null
}
const getWorkingHours = (time) => {
    if (!!time && typeof time === "object" && Object.keys(time).length > 0) {
        let result = ' من '
        result += time?.from?.hour
        result += ':'
        result += time?.from?.minute
        result += ' إلى '
        result += time?.to?.hour
        result += ':'
        result += time?.to?.minute

        return result;
    } else {
        if (typeof time === "string") {
            return time
        } else {
            return null
        }
    }
}
const getDateFromObject = ({ date, req }) => {
    if (typeof date === 'string') {
        return getDateFromString(date, 'iYYYYiMMiDD', req)
    }
    if (date?.year && date?.month && date?.day) {
        if (date?.day < 10) {
            date.day = '0'.concat(date?.day)
        }
        if (date?.day?.length > 2) {
            date.day = date?.day.substring(date?.day.length - 2, date?.day.length)
        }

        if (date?.month < 10) {
            date.month = '0'.concat(date?.month)
        }
        if (date?.month?.length > 2) {
            date.month = date?.month.substring(date?.month.length - 2, date?.month.length)
        }

        return moment(''.concat(date?.year).concat(date?.month).concat(date?.day), "iYYYYiMMiDD").format(req);

    } else {
        return date?.year + date?.month + date?.day
        // return null
    }
}
const getDateFromString = (date, format, req) => {
    if (typeof date === "string" && date.indexOf('/') > -1) {
        return date
    } else if (!date) {
        return null
    }
    return moment(date, format).format(req);
}
const extractDateToObject = (dateObject) => {
    console.log(`extractDate =============================> ${dateObject}`)
    const expDate = {}
    if (!!dateObject) {
        let returned = getDateFromString(dateObject, 'iYYYYiMMiDD', 'iDD');
        if (!isNaN(returned)) {
            expDate.day = Number.parseInt(returned)
        }
        returned = getDateFromString(dateObject, 'iYYYYiMMiDD', 'iMM');
        if (!isNaN(returned)) {
            expDate.month = Number.parseInt(returned)
        }
        returned = getDateFromString(dateObject, 'iYYYYiMMiDD', 'iYYYY');
        if (!isNaN(returned)) {
            expDate.year = Number.parseInt(returned)
        }
    }
    console.log(`expDate =============================> ${JSON.stringify(expDate)}`)
    return expDate
}

export { getDateFromObject, dateFormatter, extractDateToObject, getDocId, getDateFromString, getWorkingHours };