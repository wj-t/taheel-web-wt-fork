export const LICENSE_FORM_TYPES = {
    ALL: "all",
    NEW: "new",
    EDIT: "edit",
    RENEW: "renew",
    DRAFT: "draft"
}
export const REQUEST_STATUS = {
    COMPLETED: -1,
    REJECTED: -4,
    CANCELED: -2,
    DRAFT: -3,
    TRANS_CENTER_REQ: 4,
    TRANS_OWNERSHIP_REQ: 5,
    RETERNED_REQ: 6,
    CANCELED_REQ: 7,
}
export const REQUEST_TYPES = {
    TEMPOR: 1,
    FINAL: 2,
    RENEW: 3,
    TRANS_CENTER: 4,
    TRANS_OWNERSHIP_REQ: 5,
    PROGRAME_REG_REQ: 6,
    CANCELING_APPROVALS: 7,
}
export const OWNER_TYPE = {
    NATURAL_TYPE: "1",
    LEGAL_TYPE: "2"
}