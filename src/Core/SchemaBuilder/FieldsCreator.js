import TextField from './FieldsInputs/TextField'
import RadioButtonField from './FieldsInputs/RadioButtonField'
import CheckboxField from './FieldsInputs/CheckboxField'
import SelectField from './FieldsInputs/SelectField'
import ButtonField from './FieldsInputs/ButtonField'
import TypographyField from './FieldsInputs/TypographyField'
import PropTypes from 'prop-types'
//import { useTranslation } from 'react-i18next'
//import { useStyles } from '../../../styles.js'
import { getOptions } from './Utils/CoreUtils'
import { arrangeFieldType } from './Utils/CoreUtils'
import { ConditionComp } from 'src/Modules/CenterServices/FinalLicense/Utils/finalLicenseUtil'
import MapContainer from '../Components/MapContainer'
import { getAddressFromObject } from 'src/Modules/CenterServices/TransferCenterOwnership/Utils/FormateJson'
import ContentField from './FieldsInputs/ContentField'
import { ConditionDependOn } from 'src/Modules/CenterServices/TemporaryLicense/Utils/temporayLicenseUtil'

export default function FieldsCreator({ lookupObject, schema, fieldsName, sectionNames, values, setField, isLoading, formType, setErrMessage }) {
    //const [t] = useTranslation('common')
    //const classes = useStyles()
    let tLabel = '', style = '', labelRootStyle = '', labelshrinkStyle = '', fieldType = '', sectionName = '', lastLength = null
    let fieldComponents = []
    const Components = { TextField, RadioButtonField, CheckboxField, SelectField, ButtonField, TypographyField }
    let newSchema = []

    if (!!fieldsName) {
        [].concat(fieldsName).map(fieldName => {
            newSchema = newSchema.concat(schema.filter(field => field?.name === fieldName)[0])
        })
    } else if (!!sectionNames) {
        [].concat(sectionNames).map(secName => {
            newSchema = newSchema.concat(schema.filter(field => field?.sectionName?.id === secName))
        })
    } else {
        newSchema = schema
    }
    newSchema.sort((a, b) => (!!a.sectionName?.order ? a.sectionName.order : 999) - (!!b.sectionName?.order ? b.sectionName.order : 999)).map(field => {
        if (!!field) {
            //if (t('lang') === 'ar') { // setting the properities for lang change
            tLabel = field.label.ar
            //style = classes.inputStyleAr
            //labelRootStyle = classes.labelRootAr
            //labelshrinkStyle = classes.shrinkAr
            /*} else {
                tLabel = field.label.en
                style = classes.inputStyleEn
                labelRootStyle = classes.labelRootEn
                labelshrinkStyle = classes.shrinkEn
            }*/
            if (!!field['sectionName'] && sectionName.id !== field.sectionName.id) {
                fieldComponents = fieldComponents.filter(n => n)
                lastLength = !!lastLength ? lastLength : 1
                if (lastLength === fieldComponents.length) fieldComponents.pop()
                sectionName = field.sectionName
                const secitonLable = !!sectionName.labelFunc ? sectionName.labelFunc(values) : sectionName?.label?.ar
                fieldComponents.push(<TypographyField type='HeadText' tLabel={secitonLable} />)
                lastLength = fieldComponents.length
            }
            field.options = !!lookupObject ? getOptions(lookupObject, field) : field.options
            field = { ...field, tLabel, style, labelRootStyle, labelshrinkStyle, values, setField, isLoading }
            let Component
            fieldType = field['type']
            if (formType === 'view') {
                if (!!field.depndOn) {
                    fieldComponents.push(
                        <ConditionDependOn schema={field} values={values}  >
                            <ContentField {...field} />
                        </ConditionDependOn >
                    )
                } else {
                    if (field.type === 'Map') {
                        const addressValue = field.valueFunc(values)
                        if (!!addressValue) { fieldComponents.push(<MapContainer isLoading={isLoading} setErrMessage={(errMessage) => { setErrMessage(errMessage) }} values={{ address: addressValue }} pauseMarker={true} title={tLabel} showChipAreaPicker={true} />) }
                    } else {
                        fieldComponents.push(ContentField({ ...field }))
                    }
                }
            } else {
                fieldType = arrangeFieldType(fieldType)
                Component = Components[fieldType]
                !!Component ? fieldComponents.push(<Component {...field} />) : fieldComponents.push(<TypographyField textTitle={'input type not recognized! ' + field['name'] + ' with type ' + field['type']} />)
            }
        }
    })
    fieldComponents = fieldComponents.filter(n => n)
    if (lastLength === fieldComponents.length) fieldComponents.pop()

    return (fieldComponents)

}
FieldsCreator.propTypes = {
    labelRootStyle: PropTypes.object,
    tLabel: PropTypes.string,
    handleChange: PropTypes.func,
    gridSize: PropTypes.string,
    type: PropTypes.string,
    name: PropTypes.string,
    value: PropTypes.string,
    multiline: PropTypes.bool,
    isLoading: PropTypes.bool,
}