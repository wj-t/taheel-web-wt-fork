export default {
    TEXT_FIELD: 'TextField',
    RADIO_BUTTON_FIELD: 'RadioButtonField',
    CHECKBOX_FIELD: 'CheckboxField',
    SELECT_FIELD: 'SelectField',
    BUTTON_FIELD: 'ButtonField',
    TYPOGRAPGY_FIELD: 'TypographyField',
    FORM_FIELD: 'FormField',
    DATA_TABLE: 'DataTable',
    FILE_UPLOADER_FIELD: 'FileUploaderField',// not implemented yet
    FILE_FILED: 'file', // to view and download /  implemented in DownloadButt and FormField
    FILE_TABLE: 'fileTable', // to view and download /  implemented in DownloadButt and FormField
    DATE_PICKER_FIELD: 'DatePickerField', // not implemented yet
    MAP_CONTAINER: 'MapContainer',
}