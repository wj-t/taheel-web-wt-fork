/* eslint-disable */
import { useState, useEffect, useMemo } from 'react';

import { getCurrentUser } from 'src/Core/Utils/UserLocalStorage';
import { APIRequest } from 'src/Core/API/APIRequest';
import OrdersSchema from '../Schema/CenterRequestsSchema'
import TableCreator from 'src/Core/SchemaBuilder/TableCreator';
import { TablePaginationObject } from 'src/Core/SchemaBuilder/Utils/TablePagination';
import TableDataViewEnum from "src/Core/SchemaBuilder/Utils/TableDataViewEnum";
import { LICENSE_FORM_TYPES, REQUEST_STATUS } from 'src/Core/Utils/enums'
import { useNavigate } from 'react-router';
import { getMyTasksFun } from 'src/Modules/CenterServices/API/ServicesApi'
import { useLocation } from 'react-router-dom'
import { getTaheelRequestsFun } from 'src/Modules/CenterServices/API/ServicesApi'
import { TabPanel } from 'src/Core/SchemaBuilder/FieldsInputs/TabPanel'
import AlertDialog from 'src/Core/Components/AlertDialog';

const CenterRequests = (props) => {
    const location = useLocation()
    const navigate = useNavigate();
    const [loading, setLoading] = useState(true);
    const [totalCount, setTotalCount] = useState();
    const [taheelRequests, setTaheelRequests] = useState([]);
    const [errMessage, SetErrMessage] = useState('')
    const [dialogContent, setDialogContent] = useState("");
    const [isOpen, setOpen] = useState(false);
    const TPObject = TablePaginationObject(TableDataViewEnum.ALL_DATA)
    const tabsInfo = [
        {
            tableTitle: 'عرض الجميع',
        },
        {
            tableTitle: 'المسودات',
        },
        {
            tableTitle: 'الطلبات المعادة',
        },
        {
            tableTitle: 'طلبات نقل الملكية',
        },
        // {
        //     tableTitle: 'طلبات الإلغاء',
        // },
    ]
    const pageTitle = 'الطلبات'
    const [value, setValue] = useState(0);

    const paramData = useMemo(() => {
        return {
            batchSize: TPObject.pagination.batchSize,
            startIndex: TPObject.pagination.startIndex
        }
    }, [TPObject.pagination.batchSize, TPObject.pagination.startIndex])

    const handlePopupClick = (content) => {
        setDialogContent(content)
        setOpen(true);
    }

    const handleClose = () => {
        setOpen(false);
    };

    useEffect(async () => {
        SetErrMessage('')
        setLoading(true)
        const { email } = getCurrentUser();
        let getTaheelRequestsRs;

        getTaheelRequestsRs = await getTaheelRequestsFun({ startIndex: paramData.startIndex, batchSize: paramData.batchSize });
        let response = {};
        if (!getTaheelRequestsRs.isSuccessful) {
            setLoading(false);
            SetErrMessage(getTaheelRequestsRs.message)
            response = { isSuccessful: false, message: getTaheelRequestsRs.message };
        } else {
            let allData = []
            const data = getTaheelRequestsRs.responseBody.data.requests;
            console.log('Details', data)

            const drafts = data.filter(r => r.status === REQUEST_STATUS.DRAFT)
            const filteredExtReq = data.filter(d => d.status === REQUEST_STATUS.RETERNED_REQ);
            const filteredOwnership = data.filter(d => d.typeId === REQUEST_STATUS.TRANS_OWNERSHIP_REQ);
            const canceledReq = data.filter(d => d.typeId === REQUEST_STATUS.CANCELED_REQ);
            allData[0] = data
            allData[1] = drafts
            allData[2] = filteredExtReq
            allData[3] = filteredOwnership
            //allData[4] = canceledReq
            setTaheelRequests(allData);
            setLoading(false);
        }

        return response;
    }, [paramData.batchSize, paramData.startIndex]);
    return (

        <>
            {tabsInfo.map((t, idx) => {
                return (
                    <>
                        <TabPanel key={idx} value={value} index={idx}>
                            <AlertDialog dialogContent={dialogContent} dialogTitle="الملاحظات" open={isOpen} onClose={handleClose} acceptBtnName="تم" />
                            <TableCreator tableStyle={{ padding: "20px", minHeight: "100%" }} key={idx} pageTitle={pageTitle} tableTitle={tabsInfo} useValue={[value, setValue]} tableShcema={{ ...OrdersSchema({ navigate, handlePopupClick }), actions: '' }} dataTable={taheelRequests[idx]} totalCount={taheelRequests[idx]?.length} loading={loading} TPObject={TPObject} errMessage={errMessage} />
                        </TabPanel>
                    </>
                )
            })}
        </>
    )
}

export default CenterRequests;