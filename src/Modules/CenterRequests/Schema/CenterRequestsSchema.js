/* eslint-disable */
import { v4 as uuid } from 'uuid';
import {
    IconButton,
    Chip,
    colors,
    InputAdornment,
    Tooltip
} from '@material-ui/core';
import InfoIcon from '@material-ui/icons/Info';
import HistoryOutlinedIcon from '@material-ui/icons/HistoryOutlined';
import ErrorOutlineIcon from '@material-ui/icons/ErrorOutline';
import DoneIcon from '@material-ui/icons/Done';
import DraftsTwoToneIcon from '@material-ui/icons/DraftsTwoTone';
import IconsList from 'src/Core/SchemaBuilder/FieldsInputs/IconsList';
import { LICENSE_FORM_TYPES, REQUEST_STATUS, REQUEST_TYPES } from 'src/Core/Utils/enums'

const CommentIconWithPopup = ({ values, handlePopupClick }) => (
    !!values.comment &&//title="الملاحظات"
    <IconButton aria-label="Remarks" onClick={() => handlePopupClick(values.comment)}>
        <Tooltip title={values.comment} style={{ maxWidth: 'none' }}>
            <InfoIcon />
        </Tooltip>
    </IconButton>

)

const ChipComponentsForStatus = ({ values }) => {
    const status = values.status
    const statusName = values.statusName?.statusNameReact
    if (status === REQUEST_STATUS.COMPLETED) {
        return (
            <Chip
                label={statusName}
                variant="outlined"
                size="medium"
                icon={<DoneIcon sx={{ color: '#43A047 !important' }} />}
                sx={{
                    color: colors.green[600],
                    borderColor: colors.green[600],
                }}
            />
        );
    }
    else if (status === REQUEST_STATUS.CANCELED || status === REQUEST_STATUS.REJECTED) {
        return (
            <Chip
                label={statusName}
                variant="outlined"
                size="medium"
                icon={<ErrorOutlineIcon sx={{ color: '#e53935 !important' }} />}
                sx={{
                    color: colors.red[600],
                    borderColor: colors.red[600],
                }}
            />
        );
    }
    else if (status === REQUEST_STATUS.DRAFT) {
        return (
            <Chip
                label={statusName}
                variant="outlined"
                size="medium"
                icon={<DraftsTwoToneIcon sx={{ color: 'grey !important' }} />}
                sx={{
                    color: colors.grey[600],
                    borderColor: colors.grey[600],
                }}
            />
        );
    } else if (status === REQUEST_STATUS.RETERNED_REQ) {
        return (
            <Chip
                label={statusName}
                variant="outlined"
                size="medium"
                icon={<HistoryOutlinedIcon sx={{ color: '#fb8c00 !important' }} />}
                sx={{
                    color: colors.orange[600],
                    borderColor: colors.orange[600],
                }}
            />
        )
    }
    return (
        <Chip
            label={statusName}
            variant="outlined"
            size="medium"
            icon={<HistoryOutlinedIcon sx={{ color: '#fb8c00 !important' }} />}
            sx={{
                color: colors.orange[600],
                borderColor: colors.orange[600],
            }}
        />
    );
};

const getRequestValues = (navigate, taskType, data) => {
    let navigatinURL = '', draftFormType = '', fromDraft = false
    if (data.status === REQUEST_STATUS.DRAFT) {
        if (taskType.trim() === 'إنشاء رخصة نهائية') {
            navigatinURL = '/center-services/finallicense'
            draftFormType = LICENSE_FORM_TYPES.NEW
        }
        else if (taskType.trim() === 'تجديد رخصة') {
            navigatinURL = '/center-services/finallicense'
            draftFormType = LICENSE_FORM_TYPES.RENEW
        }
        else if (taskType.trim() === 'نقل مركز') {
            navigatinURL = '/center-services/transfercenter'
            draftFormType = LICENSE_FORM_TYPES.RENEW
        }
        else if (taskType.trim() === 'نقل ملكية') {
            navigatinURL = '/center-services/transfercenterownership'
            draftFormType = LICENSE_FORM_TYPES.DRAFT
        }
        else if (taskType.trim() === 'نقل ملكية') {
            navigatinURL = '/center-services/transNewOnership'
            draftFormType = LICENSE_FORM_TYPES.NEW
        } else if (taskType.trim() === 'برامج مركز') {
            navigatinURL = '/center-services/UpdateProgram'
            draftFormType = LICENSE_FORM_TYPES.DRAFT
        } else if (taskType.trim() === 'إنشاء رخصة مؤقتة' || taskType.trim() === 'إنشاء موافقة مبدئية') {
            navigatinURL = '/center-services/templicense'
            draftFormType = LICENSE_FORM_TYPES.DRAFT
        } else {
            navigatinURL = '/center-services/finallicense'
            draftFormType = LICENSE_FORM_TYPES.NEW
        }
        fromDraft = true
    } else if (data.typeId === REQUEST_TYPES.FINAL || data.typeId === REQUEST_TYPES.RENEW || data.typeId === REQUEST_TYPES.TEMPOR) {
        navigatinURL = '/app/centersDetails'
    } else if (data.typeId === REQUEST_TYPES.TRANS_OWNERSHIP_REQ) {
        navigatinURL = '/center-services/transferCenterOwnershipSummary'
    }
    else if (data.typeId === REQUEST_TYPES.PROGRAME_REG_REQ) {
        navigatinURL = '/center-services/ProgramRequestSummary'
    }
    else if (data.typeId === REQUEST_TYPES.CANCELING_APPROVALS) {
        navigatinURL = '/center-services/cancelInitialApprovalSummary'
    }
    else {
        navigatinURL = '/center-services/transfercentersummary'
    }

    navigate(navigatinURL, {
        state: {
            requestDetails: data,
            licenseNumber: data.centerLicenseNumber,
            formType: draftFormType,
            requestNum: data.requestNum,
            fromDraft: fromDraft
        }
    })
}

export default ({ navigate, handlePopupClick }) => {
    return {
        schema: [
            {
                id: uuid(),
                label: {
                    ar: 'رقم الطلب',
                    en: 'Orders Number'
                },
                attrFunc: (value) => {
                    if (value.status === REQUEST_STATUS.DRAFT
                        || value.typeId === REQUEST_TYPES.TRANS_CENTER
                        || value.typeId === REQUEST_TYPES.FINAL
                        || value.typeId === REQUEST_TYPES.RENEW
                        || value.typeId === REQUEST_TYPES.TEMPOR
                        || value.typeId === REQUEST_TYPES.TRANS_OWNERSHIP_REQ
                        || value.typeId === REQUEST_TYPES.PROGRAME_REG_REQ
                        || value.typeId === REQUEST_TYPES.CANCELING_APPROVALS
                    ) {
                        return (
                            <>
                                {value.requestNum}
                                <IconButton
                                    color="primary"
                                    component="span"
                                    onClick={() => {
                                        console.log(`LatestDraft :: navigate to: ${value.type}, requestNum: ${value.requestNum}`);
                                        getRequestValues(navigate, value.type, value);
                                    }}
                                >
                                    {
                                        //<IconsList iconType={data.status === REQUEST_STATUS.DRAFT ? "EditIcon" : "VisibilityIcon"} />
                                    }
                                    <IconsList iconType={"VisibilityIcon"} />
                                </IconButton>
                            </>)
                    }
                    else {
                        return value.requestNum
                    }
                },
                type: 'Text',
            },
            {
                id: uuid(),
                label: {
                    ar: 'اسم المركز',
                    en: 'Center Name'
                },
                name: 'centerName',
                attrFunc: (value) => value ? value : '--',
                type: 'Text',
            },
            {
                id: uuid(),
                label: {
                    ar: 'نوع الطلب',
                    en: 'Request Type'
                },
                name: 'type',
                type: 'Text',
            },
            {
                id: uuid(),
                label: {
                    ar: 'تاريخ الطلب',
                    en: 'Order request date'
                },
                name: 'requestDate',
                type: 'Text',
            },
            {
                id: uuid(),
                label: {
                    ar: 'حالة الطلب',
                    en: 'Order status'
                },
                attrFunc: (values) => {
                    return (
                        <>
                            <ChipComponentsForStatus values={values} />
                            <CommentIconWithPopup values={values} handlePopupClick={(comment) => handlePopupClick(comment)} />
                        </>
                    )
                },
                type: 'Text'
            }],
    }
}
