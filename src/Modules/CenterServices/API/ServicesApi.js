import { APIRequest } from "src/Core/API/APIRequest";
import { LICENSE_FORM_TYPES } from 'src/Core/Utils/enums'

const getMyTasksFun = async (userEmail) => {
  const url = 'taheel-apis-utilities-GetGetExternalUserTasks-v2';
  const queryParams = { userEmail, taskStatus: 0 };
  const response = await APIRequest({ url, queryParams });
  return response;
};

const cancelTCRequest = async (externalUserTaskID, licenseNumber) => {
  const url = '/taheel-apis-services-initiate-center-location-change-request';
  const requestBody = { serviceStatus: 2, externalUserTaskID, cancel: true, center: { licenseNumber } };
  const response = await APIRequest({ url, requestBody });
  return response;
}

const getTaheelRequestsFun = async ({ startIndex = 1, batchSize = "", type, requestTypeId, licenseNumber, status, 是统计吗 }) => {
  const url = 'taheel-apis-records-getRequests-v2';
  let requestBody = { startIndex, batchSize, licenseNumber, requestTypeId, status, 是统计吗 };
  console.log(`ORDERS ::1 queryParams ${JSON.stringify(requestBody)}`)
  if (type === LICENSE_FORM_TYPES.DRAFT) {
    requestBody.status = 4
  }
  console.log(`ORDERS ::2 queryParams ${JSON.stringify(requestBody)}`)
  const response = await APIRequest({ url, requestBody });
  return response;
};

const getRequestDetails = async (reqNum) => {
  const url = '/taheel-apis-records-RequestDetails-v2';
  const queryParams = { reqNum };
  const response = await APIRequest({ url, queryParams });
  return response;
}
const validateCompanyFunc = async (CRNumber) => {
  const url = "taheel-apis-utilities-validateCompany-v2"
  const requestBody = {
    CRNumber: CRNumber,
  };
  const response = await APIRequest({ url, requestBody });
  return response;
}
const getCentersForFinalNoExpired = async (userEmail) => {
  const url = 'taheel-apis-records-getCenters-v2';
  // const queryParams = { userEmail, isExpired: false, licenseType: 'رخصة مؤقتة' };
  const queryParams = { userEmail, isExpired: false, isEligibleForFinal: true, licenseType: "2" };
  // const queryParams = { userEmail, forRenewal: true};
  const response = await APIRequest({ url, queryParams });
  // console.log("response===============> " + JSON.parse(response));
  return response;
};

const getMunicipalLicenseNoApi = async (CRNumber) => {
  const url = 'tt-api-utilities-getmomralicense';
  const requestBody = { CrNumber: CRNumber };
  const response = await APIRequest({ url, requestBody });
  return response;
};

const CentertDetails = async (licenseNumber) => {
  const url = "taheel-apis-records-CentertDetails-v2"
  const queryParams = { licenseNumber };
  const response = await APIRequest({ url, queryParams });
  return response;
}
const deleteDraftFunc = async ({ requestNum, email }) => {
  const url = 'taheel-apis-services-DeleteTempLicenseDraft';
  const requestBody = {
    requestNumber: requestNum,
    userCreationEmail: email,
  };
  const response = await APIRequest({ requestBody, url });
  return response;
};


export { getMyTasksFun, cancelTCRequest, getRequestDetails, getCentersForFinalNoExpired, getTaheelRequestsFun, validateCompanyFunc, getMunicipalLicenseNoApi, CentertDetails, deleteDraftFunc }