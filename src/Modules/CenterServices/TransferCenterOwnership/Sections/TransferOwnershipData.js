/* eslint-disable */
import {
    Alert, Button, CardContent, CircularProgress, Grid, Link, MenuItem, Typography
} from '@material-ui/core';
import { Select, TextField as TextFieldFinal } from 'final-form-material-ui';
import { useState } from 'react';
import { Field } from 'react-final-form';
import { OnChange } from 'react-final-form-listeners';
import Calendar from 'src/Core/Components/calendar';
import { OWNER_TYPE } from 'src/Core/Utils/enums';
import { getCurrentUser } from 'src/Core/Utils/UserLocalStorage';
import { verifyEmailAndIqamaNum } from 'src/Modules/UserAuthentication/Registration/API/RegistrationAPI';
import { downloadTaheelDoc } from '../../../Account/API/AccountApi';
import { validateCitizenFunc, validateCompanyFunc } from '../../FinalLicense/API/finalLicenseAPI';
import FileUploaderComp from '../../FinalLicense/Components/FileUploader';
import { ContentField } from '../../FinalLicense/Utils/finalLicenseUtil';

const TransferOwnershipData = ({ setField, Condition, values, setIsEnableNextBtn }) => {
    const [loading, setLoading] = useState(false);
    const [checkData, setCheckData] = useState(false);
    const [errMessage, setErrMessage] = useState('');
    const { email, idNumIqamaNum } = getCurrentUser();

    function numberToDay(day) {
        return ('0' + day).slice(-2);
    }
    const setDocument = (name, docID, multipleFile) => {
        if (!multipleFile)
            setField(name, [docID])
        else {
            multipleFileDocs.push(docID)
            setField(name, multipleFileDocs)
        }
    }
    const Download = async (values) => {
        const downloadDoc = await downloadTaheelDoc("28807", "WaiverDeclarationTemplate");
    }
    const validateData = async () => {
        console.log('values.NewCRNumber+_+_+_+_+_+_+_+', JSON.stringify(values))

        setLoading(true);
        setErrMessage('');
        const { day, month, year } = values;
        const birthDate = year + '' + numberToDay(month) + numberToDay(day);

        if (!values.NewCRNumber && values.requestType === OWNER_TYPE.LEGAL_TYPE) {
            console.log('i am here +_+_+_+_+_+_+_+')
            setErrMessage(' يرجى إدخال رقم السجل التجاري للمالك الجديد');
            setLoading(false)
        } else if (values.requestType == OWNER_TYPE.LEGAL_TYPE) {
            console.log('i am here +_+_+_+_+_+_+_+2')
            const validateCompanyRs = await validateCompanyFunc(values.NewCRNumber)
            if (!validateCompanyRs.isSuccessful) {
                setIsEnableNextBtn(false);
                setErrMessage(validateCompanyRs.message);
                setLoading(false)
            } else {
                const { CRName, crCommissioner, Activities, IssueDate, ExpiryDate } = validateCompanyRs.responseBody.data;
                const idNumber = crCommissioner.natId;
                const verifyEmailAndIqamaNumRs = await verifyEmailAndIqamaNum({ idNumber });
                if (verifyEmailAndIqamaNumRs.isSuccessful) {
                    //   return { isSuccessful: false, message: verifyEmailAndIqamaNumRs.message };
                    setErrMessage('تفيد سجلاتنا بأن حساب المالك الجديد غير نشط، أو معطل، أو غير مسجل في المنصة. نرجو التأكد من هوية المالك الجديد');
                    setLoading(false)
                    return;
                }
                setField('companyName', CRName);
                setField('commissionerName', crCommissioner.name);
                setCheckData(true);
                setLoading(false);
                setField("agree", [true])
                setField("natId", crCommissioner.natId)
                setField("BirthDateH", crCommissioner.BirthDateH)
                setField("comEmail", crCommissioner.email)
                setField("mobile", crCommissioner.mobile)
                setField('crActivityType', Activities);
                setField('crIssueDate', IssueDate);
                setField('crExpirationDate', ExpiryDate);
                return;
            }
        } else if (!values.idNo && values.requestType === OWNER_TYPE.NATURAL_TYPE) {

            setErrMessage('يرجى إدخال رقم الإقامة');
            setLoading(false)
            return;
        }
        if (!values.idNo?.startsWith('1') && values.requestType === OWNER_TYPE.NATURAL_TYPE) {
            setErrMessage('تشير سجلاتنا أن صاحب الهوية غير سعودي/سعودية الجنسية');
            setLoading(false)
            return;
        }
        if (values.idNo?.length != 10 && values.requestType === OWNER_TYPE.NATURAL_TYPE) {
            setErrMessage("تشير سجلاتنا أن رقم الهوية/ الإقامة المُدخَل غير صحيح. الرجاء التأكد من صحة الرقم");
            setLoading(false);
            return;
        }
        else if (!values.day && !values.month && !values.year && values.requestType === OWNER_TYPE.NATURAL_TYPE) {

            setErrMessage('يرجى إدخال تاريخ الميلاد');
            setLoading(false)
        }

        else if (values.requestType === OWNER_TYPE.NATURAL_TYPE) {
            if (values.idNo == idNumIqamaNum) {
                setErrMessage('لا يجوز إدخال رقم الهوية/ الإقامة الخاص بك. الرجاء إدخال الرقم الخاص بالمالك الجديد');
                setLoading(false)
                return;
            }
            const verifyEmailAndIqamaNumRs = await verifyEmailAndIqamaNum({ idNumber: values.idNo });
            if (verifyEmailAndIqamaNumRs.isSuccessful) {
                setErrMessage('تفيد سجلاتنا بأن حساب المالك الجديد غير نشط، أو معطل، أو غير مسجل في المنصة. نرجو التأكد من هوية المالك الجديد');
                setLoading(false)
                return;
            }
            const validateCitizenRs = await validateCitizenFunc(values.idNo, birthDate, false)
            if (!validateCitizenRs.isSuccessful) {
                setErrMessage(validateCitizenRs.message);
                setLoading(false)
                return
            } else {
                const { firstName, secondName, thirdName, fourthName } = validateCitizenRs.responseBody.data.name;
                setField('companyName', firstName + ' ' + secondName + ' ' + thirdName + ' ' + fourthName);
                setField('commissionerName', null);
                setCheckData(true);
                setLoading(false);
                setField("agree", [true])
                setLoading(false)
                return;
            }
        }
    };
    const handleOnChange = () => {
        console.log('handleOnChange_+_+_+_+_+_+_+')

        setField('companyName', null);
        setField('commissionerName', null);
        setField("agree", [false])
        setField("natId", null)
        setField("BirthDateH", null)
        setField("comEmail", null)
        setField("mobile", null)
        setField('crActivityType', null);
        setField('crIssueDate', null);
        setField('crExpirationDate', null);
        setCheckData(false);

    };

    return (
        <CardContent>
            <Typography
                color="textPrimary"
                gutterBottom
                mb={4}
                mt={2}
                variant="h4"
            >
                معلومات المالك الجديد
            </Typography>
            <Grid
                container
                mt={4}
                spacing={3}
            >
                <Grid item md={12} xs={12}>
                    {errMessage && (
                        <Alert variant="outlined" severity="error">
                            {errMessage}
                        </Alert>
                    )}
                </Grid>
                <Grid
                    item
                    md={6}
                    xs={12}
                    className="custom-label-field"
                >
                    <Field
                        fullWidth
                        label="صفة المالك*"
                        name="requestType"
                        component={Select}
                        required
                        variant="outlined"
                        dir="rtl"
                        className="custom-field"
                        formControlProps={{ fullWidth: true }}
                        disabled={loading}
                    >
                        <MenuItem value={OWNER_TYPE.NATURAL_TYPE} selected>صفة طبيعية</MenuItem>
                        <MenuItem value={OWNER_TYPE.LEGAL_TYPE} >صفة إعتبارية</MenuItem>
                    </Field>
                    <OnChange name="requestType">
                        {(value, previous) => {
                            console.log(`odai + ${value} + ${previous}`)
                            setField('NewCRNumber', null)
                            setField('idNo', null)
                            setField('day', null)
                            setField('month', null)
                            setField('year', null)
                            handleOnChange()
                            values.crNo = ''; // eslint-disable-line no-param-reassign
                        }}
                    </OnChange>
                </Grid>
                <Grid
                    item
                    md={6}
                    xs={12}
                    className="custom-label-field"
                    display={(values.requestType === OWNER_TYPE.NATURAL_TYPE) ? 'flex' : 'none'}
                >
                    <Field
                        fullWidth
                        required
                        label="رقم الهوية/الإقامة للمالك الجديد"
                        name="idNo"
                        component={TextFieldFinal}
                        type="text"
                        variant="outlined"
                        dir="rtl"
                        className="custom-field"
                        disabled={loading}
                    />
                    <OnChange name="idNo">
                        {(value, previous) => {
                            values.idNo = value?.replace(/\D/g, '')
                            values.idNo = values?.idNo?.substring(0, 10)
                            handleOnChange(value, previous);
                        }}
                    </OnChange>
                </Grid>
                <Grid
                    item
                    md={12}
                    xs={12}
                    display={(values.requestType === OWNER_TYPE.NATURAL_TYPE) ? 'flex' : 'none'}
                >
                    <Typography> تاريخ الميلاد</Typography>
                </Grid>
                <Grid
                    item
                    md={12}
                    xs={12}
                    display={(values.requestType === OWNER_TYPE.NATURAL_TYPE) ? 'flex' : 'none'}
                >
                    < Calendar
                        disabled={loading}
                        FeiledWidth={4}
                        fieldName={null} />
                </Grid>
                <Grid
                    item
                    md={6}
                    xs={12}
                    className="custom-label-field"
                    display={(values.requestType === OWNER_TYPE.LEGAL_TYPE) ? 'flex' : 'none'}
                >
                    <Field
                        fullWidth
                        required
                        label="رقم السجل التجاري للمالك الجديد"
                        name="NewCRNumber"
                        component={TextFieldFinal}
                        type="text"
                        variant="outlined"
                        dir="rtl"
                        className="custom-field"
                        disabled={loading}
                    />
                    <OnChange name="NewCRNumber">
                        {(value, previous) => {
                            handleOnChange(value, previous);
                        }}
                    </OnChange>
                </Grid>
                <Grid
                    item
                    md={6}
                    xs={12}
                >
                    <Button
                        startIcon={loading ? <CircularProgress size="1rem" /> : null}
                        variant='outlined'
                        type="button"
                        disabled={loading}
                        sx={{
                            height: 55,
                            backgroundColor: 'white',
                            width: '100%',
                            color: '#3c8084',
                            ':hover': {
                                backgroundColor: '#3c8084',
                                color: 'white',
                            }
                        }}
                        onClick={validateData}
                    >
                        تحقق
                    </Button>
                </Grid>
            </Grid>
            <Grid
                container
                mt={3}
                mb={3}
            >
                <Condition is={checkData}>
                    <Grid
                        container
                        // spacing={3}
                        mt={3}
                        mb={3}
                    >
                        <Grid
                            item
                            lg={6}
                            md={6}
                            xs={12}
                        >
                            < ContentField label='اسم المالك الجديد' value={values.companyName} />
                        </Grid>
                        <Grid
                            item
                            lg={6}
                            md={6}
                            xs={12}
                        >
                            {!!values.commissionerName && < ContentField label='  اسم المفوض' value={values.commissionerName} />}
                        </Grid>
                    </Grid>
                </Condition>
            </Grid>
            <Typography
                color="textPrimary"
                gutterBottom
                mb={4}
                mt={5}
                variant="h4"
            >
                بيانات نقل ملكية مركز
            </Typography>
            <Grid
                container
                spacing={3}
            >
                <Grid
                    item
                    md={6}
                    xs={12}
                    className="custom-label-field"
                >
                    <Field
                        fullWidth
                        label="تحديد مقر نقل ملكية"
                        name="locationType"
                        component={Select}
                        required
                        variant="outlined"
                        dir="rtl"
                        className="custom-field"
                        formControlProps={{ fullWidth: true }}
                        onChange={() => console.log('odai')}
                    >
                        <MenuItem value="2" selected>نقل ملكية مقر حالي</MenuItem>
                        <MenuItem value="1">نقل ملكية لمقر جديد</MenuItem>
                    </Field>
                    <OnChange name="locationType">
                        {(value, previous) => {
                            console.log(`odai + ${value} + ${previous}`);
                            values.crNo = ''; // eslint-disable-line no-param-reassign
                        }}
                    </OnChange>
                </Grid>
                <Grid
                    item
                    md={6}
                    xs={12}
                >
                    <Field
                        label="عقد المبايعة"
                        name="ContractOfSale"
                        component={FileUploaderComp}
                        multipleFile={false}
                        setField={setField}
                        setDocument={setDocument}
                        values={values}
                    />
                </Grid>
                <Grid
                    item
                    md={6}
                    xs={12}
                >
                    {/* <Typography gutterBottom variant="h5" component="span">
                        الضمان المالي */}
                    <Link href="#"
                        sx={{ color: '#147fbd' }}
                        onClick={(event) => {
                            event.preventDefault()
                            Download()
                        }
                        }
                    > (تنزيل نموذج إقرار التنازل) </Link>
                    {/* </Typography> */}
                    <Field
                        label="إقرار التنازل"
                        name="WaiverDeclaration"
                        component={FileUploaderComp}
                        multipleFile={false}
                        setField={setField}
                        setDocument={setDocument}
                        values={values}
                    />
                </Grid>
            </Grid>
        </CardContent>
    )
}
export default TransferOwnershipData;