/* eslint-disable no-unused-vars */
import {
    Button, CircularProgress,
    Divider, Grid, Table, TableBody, TableCell, TableHead,
    TableRow, Typography
} from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';
import { TextField } from '@mui/material';
import { Box } from '@mui/system';
import { TextField as TextFieldFinal } from 'final-form-material-ui';
import PropTypes from 'prop-types';
import { Fragment, useEffect, useState } from 'react';
import { Field } from 'react-final-form';
import PerfectScrollbar from 'react-perfect-scrollbar';
import FileUploaderComp from '../../FinalLicense/Components/FileUploader';
import { DownloadBtn } from '../../FinalLicense/Utils/finalLicenseUtil';


const OtherDocuments = ({ Condition, values, setField, setErrMessage, setIsEnableNextBtn }) => {
    useEffect(() => {
        setIsEnableNextBtn(true)
    }, [])

    const [otherDocuments, setOtherDocuments] = useState(values.otherDocuments || []);
    const [clearUploadedFileName, setClearUploadedFileName] = useState(false);
    const [isLoading, setIsLoading] = useState(false);
    const addDocuments = () => {
        if (!values.description) {
            return setErrMessage('يجب تعبئة حقل الوصف');
        } else if (!values.document) {
            return setErrMessage('يجب تعبئة حقل المرفقات');
        } else {
            setErrMessage('');
            setOtherDocuments(attchs => {
                const newAttchs = [...attchs, { description: values.description, document: values.document[0] }]
                setField('otherDocuments', newAttchs)
                return newAttchs;
            }
            );
            setField('description', null)
            setField('document', null)
            setClearUploadedFileName(true)
        }

    };
    const handleRemoveAttch = (document) => {
        setOtherDocuments(attchs => {
            const newAttchs = attchs.filter(item => (!!item.document[0] ? item.document[0] : item.document) !== (!!document[0] ? document[0] : document))
            setField('otherDocuments', newAttchs)
            return newAttchs;
        });

    };
    return (
        <>
            <Grid container spacing={1}>
                <Grid item style={{ paddingTop: '20px', paddingBottom: '20px' }} >
                    <Box>
                        <Typography variant="h4" style={{ fontWeight: 'bold' }} flexItem>
                            مرفقات أخرى
                        </Typography>
                        <Divider light flexItem />
                    </Box>
                </Grid>
                <Grid container spacing={3}>
                    <Grid item md={12} xs={12} className="custom-label-field"></Grid>
                    <Grid item md={6} xs={12} className="custom-label-field">
                        <Field
                            fullWidth
                            required
                            label="الوصف"
                            name="description"
                            component={TextFieldFinal}
                            type="text"
                            variant="outlined"
                            dir="rtl"
                            className="custom-field"
                            onChange={(e) => setField('description', e.target.value)}
                            formControlProps={{ fullWidth: true }}
                        />
                    </Grid>
                    <Grid item md={6} xs={12} className="custom-label-field">
                        <Field
                            label="الملف"
                            name="document"
                            component={FileUploaderComp}
                            multipleFile={false}
                            setField={setField}
                            resetAttachment={() => {
                                setClearUploadedFileName(false)
                                return clearUploadedFileName
                            }}
                            values={values}
                        />
                    </Grid>
                    <Grid item md={6} xs={12} className="custom-label-field">
                        <Button
                            startIcon={isLoading ? <CircularProgress size="1rem" /> : null}
                            disabled={isLoading}
                            variant="outlined"
                            type="button"
                            sx={{
                                height: 55,
                                backgroundColor: 'white',
                                width: '100%',
                                color: '#3c8084',
                                ':hover': {
                                    backgroundColor: '#3c8084',
                                    color: 'white'
                                }
                            }}
                            onClick={() => {
                                addDocuments();
                            }}
                        >
                            إضافة
                        </Button>
                    </Grid>
                </Grid>
            </Grid>
            <PerfectScrollbar >
                <Table>
                    <TableHead >
                        <TableRow>
                            <TableCell colspan="2">
                                <Grid container justifyContent="center">
                                    الوصف
                                </Grid>
                            </TableCell>
                            <TableCell colspan="2">
                                <Grid container justifyContent="center">
                                    الملف
                                </Grid>
                            </TableCell>
                            <TableCell> </TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {otherDocuments?.map((item, idx) => (
                            <Fragment key={idx}>
                                <TableRow key={idx}>
                                    <TableCell component="th" scope="row" colspan="2" >
                                        <TextField
                                            id="outlined-multiline-static"
                                            label="الوصف"
                                            multiline
                                            rows="2"
                                            defaultValue="Default Value"
                                            variant="outlined"
                                            disabled={true}
                                            value={item.description}
                                            fullWidth={true}
                                        />
                                    </TableCell>
                                    <TableCell component="th" scope="row" colspan="2" >
                                        <DownloadBtn index={idx} docID={item.document} />
                                    </TableCell>
                                    <TableCell component="th" scope="row">
                                        <Button
                                            fullWidth
                                            color="error"
                                            startIcon={<DeleteIcon />}
                                            onClick={() => handleRemoveAttch(item.document)}
                                        >حذف</Button>
                                    </TableCell>
                                </TableRow>
                            </Fragment>
                        ))}
                        {otherDocuments?.length === 0 &&
                            (<TableRow hover>
                                <TableCell colSpan={5} >
                                    <p style={{ textAlign: 'center' }} >لا توجد بيانات </p>
                                </TableCell>
                            </TableRow>)}
                    </TableBody>
                </Table>
            </PerfectScrollbar>
        </>
    );
}
export default OtherDocuments;

OtherDocuments.propTypes = {
    Condition: PropTypes.func.isRequired,
    values: PropTypes.object.isRequired,
    setField: PropTypes.func.isRequired,
    setErrMessage: PropTypes.func.isRequired,
    setIsEnableNextBtn: PropTypes.func.isRequired,
};


