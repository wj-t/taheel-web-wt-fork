/* eslint-disable */

import {
    Grid
} from '@material-ui/core';
import { useEffect, useState } from 'react';
import { useNavigate } from 'react-router';
import { useLookup, useUpdateLookup } from 'src/Core/Contexts/useLookup';
import FieldsCreator from 'src/Core/SchemaBuilder/FieldsCreator';
import NewOwnerCenterDetailsSchema from '../Schema/NewOwnerCenterDetailsSchema';
import TransferOwnershipDialog from './TransferOwnershipDialog';

const NewOwnerCenterDetails = ({ values, getCentertDetails, isLoading, setIsEnableNextBtn, setField }) => {
    const navigate = useNavigate();
    const lookupValues = useLookup()
    const refreshLookup = useUpdateLookup()

    useEffect(() => {
        lookupValues?.isEmpity && (refreshLookup())
        console.log(`values.centerLicenseNumber: ${values.centerLicenseNumber}`)
        values.isAuth && (setOpen(false))
        setIsEnableNextBtn(true);

    }, []);
    const [open, setOpen] = useState(true);
    return (
        <>
            <Grid
                container
                style={{ paddingRight: '30px' }}
            >
                <FieldsCreator schema={NewOwnerCenterDetailsSchema} lookupObject={lookupValues} values={values} isLoading={false} formType="view" />
            </Grid>
            <TransferOwnershipDialog formType='newOwnership' val={values} open={open} setOpen={(open) => setOpen(open)} handleOnSuccess={() => { setField('isAuth', true) }} onClose={() => { }} onClose={() => navigate('/center-services/transferCenterOwnershipSummary', { state: { requestNum: values.requestNum } })} />
        </>
    );
}
export default NewOwnerCenterDetails;
