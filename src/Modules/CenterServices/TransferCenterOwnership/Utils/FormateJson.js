import numeral from "numeral"
import { reverseRange } from "src/Core/Utils/utilFunctions"
import { extractDateToObject, getDateFromString, getDocId } from "src/Core/Utils/TaheelUtils"
import { REQUEST_STATUS } from "src/Core/Utils/enums"

export const getAddressFromObject = (data) => {
    var result = {}
    if (!!data?.centerLocation_r && Object.keys(data.centerLocation_r).length > 0) {
        return formatLocation(data.centerLocation_r)
    }

    result.buildNo = !!data?.buildNo ? data.buildNo : ''
    result.street = !!data?.street ? data.street + ', ' : ''
    result.area = !!data?.area ? data.area : ''
    result.postalCode = !!data?.postalCode ? data.postalCode + ', ' : ''
    result.city = !!data?.city ? data.city : ''
    result.lat = !!data?.lat ? data.lat : ''
    result.lng = !!data?.lng ? data.lng : ''

    return formatLocation(result);
}
export const formatLocation = (location) => {
    if (!location || !Object.keys(location).length) {
        return null;
    }
    var result = {}
    if (!!location.buildNo || !!location.street || !!location.area || !!location.postalCode || !!location.city)
        result.caption = `${location.buildNo} ${location.street} ${location.area} ${location.postalCode?.replace("-", " ")} ${location.city} `

    if (!!location.lat && !!location.lng) {
        result.heart = { lat: parseFloat(location.lat), lng: parseFloat(location.lng) }
    } else {
        result.heart = { lat: 24.774265, lng: 46.738586 }

    }

    return !result.caption || !result.heart ? null : result
}
const getName = (data) => {
    return data?.firstName + ' ' + data?.lastName
}
export const getGender = (value) => {
    return value === "m" ? "ذكر" : (value === "f" ? "أنثى" : value)
}
export const formateStaff = (staff) => {
    const staffTypes = ["", "معلم تربية خاصة", "أخصائي اجتماعي", "مراقب اجتماعي", "حارس", "عامل تنظيفات", "مشرف فني عام", "اخصائي نفسي و توجيه اجتماعي", "عامل رعاية شخصية", "مدير", "سائق", "مرافق سائق", "أخصائي علاج طبيعي", "أخصائي علاج وظيفي", "أخصائي نطق و تخاطب", "ممرض"]
    if (!Array.isArray(staff)) {
        return null
    }
    console.log(`=========================> staff :: ${staff}`)
    const newStaff = staff?.map(s => {
        s.fullName = s?.name
        s.staffTypes = staffTypes[s?.StaffType]
        s.staffType = staffTypes[s?.StaffType]
        s.idNumber = s?.idNumIqamaNum
        s.iqamaNo = s?.idNumIqamaNum
        s.gender = getGender(s?.gender)
        s.EducationalQualification = s?.educationQualifications
        s.cv = s?.CV
        s.day = getDateFromString(s?.birthDate, 'iYYYYiMMiDD', 'iDD')
        s.month = getDateFromString(s?.birthDate, 'iYYYYiMMiDD', 'iMM')
        s.year = getDateFromString(s?.birthDate, 'iYYYYiMMiDD', 'iYYYY')
        return s;
    })
    return newStaff.map(ns => ns)
}
const formateGetRequestDetails = (data) => {
    const center = data.center
    const processVariablesDump = data.processVariablesDump
    const request = data.request
    const requestDetails = data.requestDetails
    const result = {
        isDraft: false,
        isAuth: true,
        page: 0,
        OperationalPlan: [(center?.centerInfo_r?.operationPlan || center?.centerInfo_r?.operationPlan?.id)],
        ExecutivePlan: [(center?.centerInfo_r?.executivePlan || center?.centerInfo_r?.executivePlan?.id)],
        SecurityReport: [(center?.centerInfo_r?.securityReport || center?.centerInfo_r?.securityReport?.id)],
        Furniture: (center?.centerInfo_r?.furniturePhoto_r?.map(d => d.Document) || center?.centerInfo_r?.furniturePhoto_r?.map(d => d.Document.id)),
        FinancialGuaranteeAtt: [(center?.centerInfo_r?.financialGuarbteeAtt || center?.centerInfo_r?.financialGuarbteeAtt?.id)],
        centerLicenseNumber: processVariablesDump?.NewCenterLocationData?.licenseNumber || processVariablesDump?.NewCenterLocationData?.centerLicense_r?.licenseNumber,
        centerType: center?.type,//check
        targetedBenificiray: center?.targetedBeneficiary,//check
        targetedServices: center?.targetedServices,//check
        ownerID: processVariablesDump?.NewCenterLocationData?.ownerID,
        ownerName: processVariablesDump?.NewCenterLocationData?.ownerName,
        ownerType: processVariablesDump?.NewCenterLocationData?.ownerType,
        centerAgeGroup: center?.ageGroup,
        centerGenderGroup: getGender(center?.targetedGender),
        requestNum: request?.requestNum,
        requestDate: request?.requestDate,
        requestType: request?.type,
        requestStatus: request?.statusName?.statusName || requestDetails?.statusName?.statusName,
        engineeringPlan: getDocId(processVariablesDump?.NewCenterLocationData?.centerInfo_r?.engineeringPlan),
        fireDepartmentLicense: getDocId(processVariablesDump?.NewCenterLocationData?.centerInfo_r?.fireDepartmentLicense),
        fireDepartmentExpD: extractDateToObject(processVariablesDump?.NewCenterLocationData?.centerInfo_r?.expirarionDateForFireDepartmentLicenseHijri),
        fireDepartmentExpDText: getDateFromString(processVariablesDump?.NewCenterLocationData?.centerInfo_r?.expirarionDateForFireDepartmentLicenseHijri, 'iYYYYiMMiDD', 'iDD/iMM/iYYYY'),
        taskId: data?.externalTaskData?.ID,
        commissionerName: processVariablesDump?.crCommissioner?.name,
        changeLocation: processVariablesDump?.otherData?.LocationofOwnershipTransfer === 'CHANGED',
        CRNumber: processVariablesDump?.crInfo_r?.crNumber || processVariablesDump?.crInfo_r?.ID?.crNumber || processVariablesDump?.NewCenterLocationData?.crInfo_r?.crNumber,
        companyName: processVariablesDump?.crInfo_r?.entityName || processVariablesDump?.crInfo_r?.ID?.entityName || processVariablesDump?.NewCenterLocationData?.crInfo_r?.entityName,
        municipLicenseNo: processVariablesDump?.crInfo_r?.MoMRA_License || processVariablesDump?.crInfo_r?.ID?.MoMRA_License || processVariablesDump?.NewCenterLocationData?.crInfo_r?.MoMRA_License,
        activities: processVariablesDump?.crInfo_r?.crActivityType || processVariablesDump?.crInfo_r?.ID?.crActivityType || processVariablesDump?.NewCenterLocationData?.crInfo_r?.crActivityType,
        crIssueDate: processVariablesDump?.crInfo_r?.crIssueDate || processVariablesDump?.crInfo_r?.ID?.crIssueDate || processVariablesDump?.NewCenterLocationData?.crInfo_r?.crIssueDate,
        crExpirationDate: processVariablesDump?.crInfo_r?.crExpirationDate || processVariablesDump?.crInfo_r?.ID?.crExpirationDate || processVariablesDump?.NewCenterLocationData?.crInfo_r?.crExpirationDate,
        beneficiariesNum: processVariablesDump?.NewCenterLocationData?.centerInfo_r?.beneficiaryCount,
        capacity: processVariablesDump?.NewCenterLocationData?.centerInfo_r?.carryingnumber,
        buildingArea: processVariablesDump?.NewCenterLocationData?.centerInfo_r?.buildingArea,
        basementArea: processVariablesDump?.NewCenterLocationData?.centerInfo_r?.basementArea,
        financialGuarantee: processVariablesDump?.NewCenterLocationData?.centerInfo_r?.financialGuarantee || center?.centerInfo_r?.financialGuarantee,
        address: formatLocation(processVariablesDump?.NewCenterLocationData?.centerLocation_r),
        area: processVariablesDump?.NewCenterLocationData?.centerLocation_r?.area, // center?.centerLocation_r?.area,
        sub: processVariablesDump?.NewCenterLocationData?.centerLocation_r?.area, // center?.centerLocation_r?.area,
        city: processVariablesDump?.NewCenterLocationData?.centerLocation_r?.city,//center?.centerLocation_r?.city,
        street: processVariablesDump?.NewCenterLocationData?.centerLocation_r?.street, //center?.centerLocation_r?.street,
        buildNo: processVariablesDump?.NewCenterLocationData?.centerLocation_r?.buildNo,//center?.centerLocation_r?.buildNo,
        lat: processVariablesDump?.NewCenterLocationData?.centerLocation_r?.lat,//center?.centerLocation_r?.lat,
        lng: processVariablesDump?.NewCenterLocationData?.centerLocation_r?.lng,//center?.centerLocation_r?.lng,
        postalCode: processVariablesDump?.NewCenterLocationData?.centerLocation_r?.postalCode,//center?.centerLocation_r?.postalCode,
        additionalNo: processVariablesDump?.NewCenterLocationData?.centerLocation_r?.additionalNo,//center?.centerLocation_r?.additionalNo,
        healthServices: processVariablesDump?.NewCenterLocationData?.isHealthCareServices ? "yes" : "no",
        healthServiceType: processVariablesDump?.NewCenterLocationData?.healthCareServices_r?.type,
        healthServiceAttachment: getDocId(processVariablesDump?.NewCenterLocationData?.healthCareServices_r?.attachment),
        healthCareServices_r: processVariablesDump?.NewCenterLocationData?.healthCareServices_r?.ID,
        otherDocuments: processVariablesDump?.otherDocuments,
        customers: processVariablesDump?.staff && formateStaff([].concat(processVariablesDump?.staff)),
        salesDoc: { id: processVariablesDump?.otherData?.ContractOfSale },
        waiverDoc: { id: processVariablesDump?.otherData?.WaiverDeclaration },
        ageGroup: center?.ageGroup,//check
        CRName: processVariablesDump?.crInfo_r?.CRName || processVariablesDump?.crInfo_r?.ID?.CRName,//check
        workingHours: processVariablesDump?.NewCenterLocationData?.workingPeriod,//check
        centerWorkingHours: processVariablesDump?.NewCenterLocationData?.workingHours,//check
        fireDepartmentLicenseExpiryDate: processVariablesDump?.NewCenterLocationData?.centerInfo_r?.expirarionDateForFireDepartmentLicenseHijri,//check
        OfficeReport: [(processVariablesDump?.NewCenterLocationData?.centerInfo_r?.engineeringPlan || processVariablesDump?.NewCenterLocationData?.centerInfo_r?.engineeringPlan?.id)],
        ownerEducationalQualifications: [(processVariablesDump?.NewCenterLocationData?.centerOwner_r?.centerOwnerEducationQualification || processVariablesDump?.NewCenterLocationData?.centerOwner_r?.centerOwnerEducationQualification?.id)],
        center: center
    }
    if (data.status === REQUEST_STATUS.COMPLETED) {
        result.licenseCreationDate = center?.centerLicense_r?.creationHijri
        result.licenseExpiryDate = center?.centerLicense_r?.expirationHijri
    }
    if (result.type != '01') {
        result.fullName = processVariablesDump?.staff?.name
        result.idNumber = processVariablesDump?.staff?.idNumIqamaNum
        result.gender = processVariablesDump?.staff?.gender
        result.birthDate = processVariablesDump?.staff?.birthDate
        result.CV = processVariablesDump?.staff?.CV //check
        result.EducationalQualifications = processVariablesDump?.staff?.educationQualifications //check
        result.MedicalReport = processVariablesDump?.staff?.medicalReport //check
        result.FirstAidCourseCompletionCertificate = processVariablesDump?.staff?.firstAidCourseCompletionCertificate //check
        result.TitleDeedOrLeaseContract = processVariablesDump?.NewCenterLocationData?.centerInfo_r?.titleDeedOrLeaseContract //check
        result.BuildingScheme = processVariablesDump?.NewCenterLocationData?.centerInfo_r?.buildingScheme//check
    }
    return result
}
const formatCenterResponseBody = (center) => {
    const initialValues = {
        agree: [false],
        isNextBtnDisabled: false,
        managersCount: 0,
        teachersCount: 0,
        centerType: center?.type,
        temporaryLicenseNum: center?.centerLicense_r?.LicenseNumber,
        licenseCreationDate: center?.centerLicense_r?.creationHijri,
        licenseExpiryDate: center?.centerLicense_r?.expirationHijri,
        ownerName: center?.centerOwner_r?.ownerName,
        ownerID: center?.centerOwner_r?.ownerID,
        centerAgeGroup: center?.ageGroup && reverseRange(center?.ageGroup),
        centerGenderGroup: center?.targetedGender,
        CRNumber: center?.crInfo_r?.crNumber,
        companyName: center?.crInfo_r?.entityName,
        // activities: center?.crInfo_r?.crActivityType,
        municipLicenseNo: center?.crInfo_r?.MoMRA_Licence,
        beneficiariesNum: center?.centerInfo_r?.beneficiaryCount,
        capacity: numeral(center?.centerInfo_r?.carryingnumber).format('0,0'),
        financialGuarantee: `${numeral(center?.centerInfo_r?.financialGuarantee).format('0,0.00')} ر.س.`,
        buildingArea: center?.centerInfo_r?.buildingArea,
        basementArea: center?.centerInfo_r?.basementArea,
        OperationalPlan: [(center?.centerInfo_r?.operationPlan || center?.centerInfo_r?.operationPlan?.id)],
        ExecutivePlan: [(center?.centerInfo_r?.executivePlan || center?.centerInfo_r?.executivePlan?.id)],
        OfficeReport: [(center?.centerInfo_r?.engineeringPlan || center?.centerInfo_r?.engineeringPlan?.id)],
        SecurityReport: [(center?.centerInfo_r?.securityReport || center?.centerInfo_r?.securityReport?.id)],
        Furniture: (center?.centerInfo_r?.furniturePhoto_r?.map(d => d.Document) || center?.centerInfo_r?.furniturePhoto_r?.map(d => d.Document.id)),
        FinancialGuaranteeAtt: [(center?.centerInfo_r?.financialGuarbteeAtt || center?.centerInfo_r?.financialGuarbteeAtt?.id)],
        healthServices: center && center.centerInfo_r && center.isHealthCareServices ? "yes" : "no",
        healthServiceType: center?.healthCareServices_r?.type,
        healthServiceAttachment: [(center?.healthCareServices_r?.attachment || center?.healthCareServices_r?.attachment?.id)],

    }
    return initialValues;
}

export { formateGetRequestDetails, formatCenterResponseBody }
