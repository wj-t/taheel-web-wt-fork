import { APIRequest } from 'src/Core/API/APIRequest';
import { getCurrentUser } from 'src/Core/Utils/UserLocalStorage';
import { getStaff } from '../Utils/TransferCenterOwnershipUtil';

const transferCenterOwnershipAPIFunc = async (data) => {
    console.log('#==> data+++' + JSON.stringify(data))
    const { email } = getCurrentUser();

    let locType = "";
    if (data.locationType == "2") {
        locType = "SAME"
    } else {
        locType = "CHANGED"
    }

    const requestBody = {
        serviceStatus: 1,
        isDraft: data.isDraft,
        userCreationEmail: email,

        otherData: {
            ContractOfSale: !!data?.ContractOfSale && data?.ContractOfSale[0],
            WaiverDeclaration: !!data?.WaiverDeclaration && data?.WaiverDeclaration[0],
            LocationofOwnershipTransfer: locType,
        },
        crCommissioner: {
            name: data.commissionerName,
            natId: data.natId,
            BirthDateH: data.BirthDateH,
            email: data.comEmail,
            mobile: data.mobile,
        },
        center: {
            centerLicense_r: {
                LicenseNumber: data.centerLicenseNumber,
            },
            centerOwner_r: {
                ownerID: data.NewCRNumber || data.idNo,
                ownerName: data.companyName,
                ownerType: data.requestType,
            }
        }
    }
    if (data.isDraft) {
        requestBody.draft_values = { centerLicenseNumber: data.licenseNumber, ...data }
    }
    let url = "taheel-apis-services-transfer-center-ownership";

    console.log('#==> requestBody ' + JSON.stringify(requestBody))
    const response = await APIRequest({ requestBody, url });
    return response;

}


const transferCenterNewOwnershipAPIFunc = async (data, isLoading) => {
    console.log('#==> transferCenterNewOwnershipAPIFunc :: data  ', data)
    const { email } = getCurrentUser();
    const { day, month, year } = data.fireDepartmentExpD;
    function numberToDay(day) {
        return ('0' + day).slice(-2);
    }
    const expiryDate = year + '' + numberToDay(month) + numberToDay(day);

    const isHealthCare = data.healthServices === "no" ? false : true

    const requestBody = {
        serviceStatus: 2,
        externalUserTaskID: data.taskId,
        isDraft: data.isDraft,
        userCreationEmail: email,
        otherDocuments: data?.otherDocuments,
        center: {
            centerLicense_r: {
                LicenseNumber: data.centerLicenseNumber,
            },
            centerOwner_r: {
                ownerID: data.ownerID,
                ownerType: data.ownerType,
                ownerName: data.ownerName,
            },
            centerInfo_r: {
                carryingnumber: data.capacity,
                beneficiaryCount: data.beneficiariesNum,
                buildingArea: data.buildingArea,
                basementArea: data.basementArea,
                engineeringPlan: !!data.engineeringPlan && data.engineeringPlan[0],
                fireDepartmentLicense: !!data.fireDepartmentLicense && data.fireDepartmentLicense[0],
                expirarionDateForFireDepartmentLicenseHijri: expiryDate
            },
            centerLocation_r: {
                city: data.city,
                area: data.sub,
                street: data.street,
                buildNo: data.buildNo,
                lat: data.lat,
                lng: data.lng,
                postalCode: data.postalCode,
                additionalNo: data.additionalNo
            },
            isHealthCareServices: isHealthCare,
            healthCareServices_r: {
                type: data.healthServices === 'yes' ? data.healthServiceType : null,
                attachment: data.healthServices === 'yes' ?
                    data.healthServiceAttachment[0]
                    : null
            }
        },
        crInfo_r: {
            ID: data.crInfo_r?.ID,
            crNumber: data.CRNumber,
            crActivityType: data.activities,
            entityName: data.companyName,
            MoMRA_License: data.municipLicenseNo,
        },
        staff: getStaff(data)
    }
    if (!data.changeLocation) {
        requestBody.centerLocation_r = {}
    }
    if (data.isDraft) {
        requestBody.draft_values = { ...data, draftType: 'NewOwnership' }
    }
    let url = "taheel-apis-services-transfer-center-ownership";

    console.log('#==> requestBody ' + JSON.stringify(requestBody))
    const response = await APIRequest({ requestBody, url });
    // const response = {isSuccessful:false, message:"DUMMY"}
    return response;
}

export { transferCenterOwnershipAPIFunc, transferCenterNewOwnershipAPIFunc };

