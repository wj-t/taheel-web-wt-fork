/* eslint-disable */
import DraftsTwoToneIcon from '@material-ui/icons/DraftsTwoTone';
import {
    Alert,
    AlertTitle, Box,
    Card,
    CardContent,
    CardHeader, CircularProgress, Container, Divider
} from '@mui/material';
import React, { useEffect, useState } from 'react';
import { useLocation, useNavigate } from 'react-router';
import AlertDialog from 'src/Core/Components/AlertDialog';
import FinalFromWizard from 'src/Core/Components/wizard/FinalFormWizard';
import { useLookup, useUpdateLookup } from 'src/Core/Contexts/useLookup';
import { LICENSE_FORM_TYPES } from 'src/Core/Utils/enums';
import { dateFormatter, reverseRange } from 'src/Core/Utils/utilFunctions';
import { getRequestDetails } from '../../API/ServicesApi';
import { calculationConditionComp, MedicalPracticeComp, personsValidation } from '../../FinalLicense/Utils/finalLicenseUtil';
import { ConditionComp } from '../../TemporaryLicense/Utils/temporayLicenseUtil';
import { transferCenterNewOwnershipAPIFunc } from '../Api/TransferCenterOwnershipAPI';
import CapacityForNewCenter from '../Sections/CapacityOfNewCenter';
import { CRNumberAndLicenses } from '../Sections/CRNumberAndLicenses';
import HealthServices from '../Sections/HealthServices';
import NewCenterAddress from '../Sections/NewCenterAddress';
import NewOwnerCenterDetails from '../Sections/NewOwnerCenterDetails';
import NewOwnerDetails from '../Sections/NewOwnerDetails';
import OtherAttachment from '../Sections/OtherAttachment';
import PersonDetials from '../Sections/staff/PersonDetials';
import Terms from '../Sections/Terms';
import { getAddressFromObject } from '../Utils/FormateJson';
import { CRNumberAndLicensesValidation, healthServicesValidation, NewAddressValidation } from '../Utils/TransferCenterOwnershipUtil';

const TransferNewOwnership = () => {

    const location = useLocation();
    const lookupValues = useLookup()
    const refreshLookup = useUpdateLookup()
    const search = location.search; // could be '?foo=bar'
    const params = new URLSearchParams(search);
    const navigate = useNavigate();
    const [errMessage, setErrMessage] = useState('');
    const [dialogContent, setDialogContent] = useState("");
    const [dialogTitle, setDialogTitle] = useState("");
    const [open, setOpen] = useState(false);
    const [isEnableNextBtn, setIsEnableNextBtn] = useState(false);
    const [isLoading, setIsLoading] = useState(true);
    const [details, setDetails] = useState({});
    const [initialValues, setInitialValues] = useState({});
    const [editMode, setEditMode] = useState(location.state?.editMode);
    const requestNum = location.state?.requestNum || params.get('requestNum');
    const requestDetails = location.state?.requestDetails
    const editRequestData = location.state?.editRequestData
    const fromDraft = location.state?.fromDraft
    const [showSummary, setShowSummary] = useState(false);
    const formType = location.state ? location.state.formType : null;
    const [changeLocation, setChangeLocation] = useState(true);
    const [center, setCenter] = useState();

    useEffect(async () => {
        lookupValues?.isEmpity && (refreshLookup())
        setErrMessage("");
        if (!requestNum && !requestDetails) {
            setDialogContent('خطأ في البيانات')
            setOpen(true)
            setIsLoading(true)
            return false;
        }
        setIsLoading(true);
        //!orderDetails && setOrderDetails(getOrderDetils(requestNum))

        setIsLoading(true);
        const getReqDetails = !!requestDetails ? requestDetails : await getRequestDetails(requestNum)
        if (!getReqDetails.isSuccessful) {
            setErrMessage(getReqDetails.message)
        } else {
            let details = getReqDetails.responseBody.requestDetails.data
            setDetails(details);
            setCenter(details.center)
            setShowSummary(true);
            console.log('details', details)
            console.log('details.center', details.center)

            const NewCenterLocationData = details.processVariablesDump.NewCenterLocationData
            const oldOwnerDetails = details.processVariablesDump.oldOwner
            const crCommissioner = details.processVariablesDump.crCommissioner
            const otherData = details.processVariablesDump.otherData
            const draft_values = details.draft_values?.draft_values
            let changeLocation = otherData?.LocationofOwnershipTransfer === 'SAME' ? false : true

            setInitialValues(initvalues => {

                initvalues = {
                    ...initvalues,
                    taskId: details?.externalTaskData?.ID,
                    ownerID: NewCenterLocationData?.ownerID,
                    ownerName: NewCenterLocationData?.ownerName,
                    ownerType: NewCenterLocationData?.ownerType,
                    commissionerName: crCommissioner?.name,
                    salesDoc: { id: otherData?.ContractOfSale },
                    waiverDoc: { id: otherData?.WaiverDeclaration },
                    changeLocation: changeLocation
                }

                if (details.draft_values?.isDraft) {
                    initvalues = {
                        ...initialValues,
                        ...details.draft_values?.draft_values,
                        address: getAddressFromObject(draft_values),
                        isDraft: false,
                        isAuth: true,
                    }
                    changeLocation = details.draft_values?.draft_values?.changeLocation
                    setChangeLocation(changeLocation)
                } else if (!!editRequestData) {
                    initvalues = {
                        ...editRequestData,
                        ...initialValues,
                    }
                }
                if (!changeLocation) {
                    initvalues.basementArea = details.center?.centerInfo_r?.basementArea
                    initvalues.buildingArea = details.center?.centerInfo_r?.buildingArea
                    initvalues.capacity = details.center?.centerInfo_r?.carryingnumber
                    initvalues.beneficiariesNum = details.center?.centerInfo_r?.beneficiaryCount
                }
                return initvalues
            })
            setChangeLocation(changeLocation)
            setIsLoading(false)
        }

    }, [])

    const handleClickOpen = (dialogContent, dialogTitle) => {
        setDialogContent(dialogContent);
        setDialogTitle(dialogTitle)
        setOpen(true);
    };

    const handleClose = (value) => {
        setOpen(false);
        navigate('/app/center-requests', { replace: true });
    };


    const onSubmit = async (values) => {
        console.log(values)
        setIsLoading(true);
        console.log("values++++++++++++", JSON.stringify(values))
        const response = await transferCenterNewOwnershipAPIFunc(values);
        console.log("response.isSuccessful", response.isSuccessful);
        if (response.isSuccessful) {
            if (values.isDraft && !!response?.responseBody?.data) {
                handleClickOpen(`${response.responseBody.data.message[0]} طلب رقم ${response.responseBody.data.requestNumber}`, '');
            }
            else {
                handleClickOpen(`${response.responseBody.data.message}`, '');
            }
        }
        else {
            setErrMessage(`${response.message}`);
            setIsLoading(false)
        }
    };

    return (
        <Container maxWidth="md">
            <Card>
                <CardHeader
                    title="نقل ملكية مركز أهلي (المالك الجديد)"
                />
                <Divider />
                {!isLoading && fromDraft &&
                    <Alert icon={<DraftsTwoToneIcon sx={{ color: 'grey !important' }} />} variant="outlined" severity="info" sx={{ marginLeft: 2, marginRight: 2, marginTop: 1, color: 'grey !important', borderColor: 'grey !important' }}>
                        <AlertTitle> مسودة رقم {requestNum}</AlertTitle>
                        {details?.request && details.request?.comment}
                    </Alert>
                }
                {errMessage && (
                    <Alert variant="outlined" severity="error">
                        {errMessage}
                    </Alert>
                )}
                <CardContent>
                    {!isLoading ?
                        <FinalFromWizard
                            initialValues={{
                                agree: [],
                                isNextBtnDisabled: false,
                                fireDepartmentExpD: {},
                                managersCount: 0,
                                teachersCount: 0,
                                centerLicenseNumber: center && center.centerLicense_r.LicenseNumber,
                                centerLicenseNumber: center && center.centerLicense_r.LicenseNumber,
                                centerAgeGroup: center && center.ageGroup && reverseRange(center.ageGroup),
                                centerGenderGroup: center
                                    && center.targetedGender &&
                                    (center.targetedGender === "m" ? "ذكر" : (center.targetedGender === "f" ? "أنثى" : "كلا الجنسين")),
                                oldCenterDetails: center && center.crInfo_r && {
                                    CRNumber: center.crInfo_r.crNumber,
                                    companyName: center.crInfo_r.entityName,
                                    activities: center.crInfo_r.crActivityType,
                                    municipLicenseNo: center.crInfo_r.MoMRA_License,
                                    city: center?.centerLocation_r?.city || center?.city,
                                    buildNo: center?.centerLocation_r?.buildNo || center?.buildNo,
                                    street: center?.centerLocation_r?.street || center?.street,
                                    sub: center?.centerLocation_r?.sub || center?.area,
                                    postalCode: center?.centerLocation_r?.postalCode || center?.postalCode,
                                    additionalNo: center?.centerLocation_r?.additionalNo || center?.additionalNo,
                                    licenseExpiryDate: dateFormatter(center?.centerLicense_r?.expirationHijri),
                                    licenseCreationDate: dateFormatter(center?.centerLicense_r?.creationHijri),
                                    centerLicenseNumber: center.centerLicense_r.LicenseNumber,
                                    targetedBenificiray: center?.targetedBeneficiary,
                                    targetedServices: center?.targetedServices,
                                    centerType: center?.type
                                },
                                targetedBenificiray: center?.targetedBeneficiary,
                                targetedServices: center?.targetedServices,
                                centerType: center?.type,
                                crInfo_r: center?.crInfo_r,
                                requestNum: requestNum,
                                requestDate: !!details?.request && details.request?.requestDate,
                                isDraft: false,
                                page: formType === LICENSE_FORM_TYPES.RENEW ? 1 : 0,
                                lookupValues: lookupValues,
                                formType: formType,
                                // Owner details
                                ...initialValues,
                            }}
                            cancelBtnFn={() => { navigate('/app/center-services-list', { replace: true }); }}
                            isEnableCancelBtn={false}
                            isEnableEndBtn={true}
                            isEnableNextBtn={isEnableNextBtn}
                            showSummary={showSummary}
                            onSubmit={onSubmit}
                        >

                            <FinalFromWizarCenterDetailsPage
                                label="بيانات المركز "
                                setField={(fieldName, fieldValue) => setField(fieldName, fieldValue)}
                                setIsEnableNextBtn={(isEnable) => setIsEnableNextBtn(isEnable)}
                            />
                            <FinalFromWizarNewOwnerDetailsPage
                                label="بيانات المالك الحالي للمركز "
                                setIsEnableNextBtn={(isEnable) => setIsEnableNextBtn(isEnable)}
                            />
                            <FinalFromWizardCRNumberAndLicensesPage
                                editMode={editMode}
                                setErrMessage={(errMessage) => setErrMessage(errMessage)}
                                setIsEnableNextBtn={(isEnable) => setIsEnableNextBtn(isEnable)}
                                label="السجل التجاري والتراخيص"
                                validate={(values) => CRNumberAndLicensesValidation(values)}
                                fromDraft={fromDraft} />

                            <FinalFromWizardCapacityOfNewCenterPage
                                label="الطاقة الإستيعابية"
                                validate={CRNumberAndLicensesValidation}
                                showSummary={showSummary}
                                setIsEnableNextBtn={(isEnable) => setIsEnableNextBtn(isEnable)}
                                changeLocation={changeLocation}
                                fromDraft={fromDraft}
                            />

                            {changeLocation && (
                                <FinalFromWizardAddressPage
                                    label="العنوان الوطني (للمبنى الجديد)"
                                    validate={(values) => NewAddressValidation(values)}
                                    setIsEnableNextBtn={(isEnable) => setIsEnableNextBtn(isEnable)}
                                    setErrMessage={(errMessage) => setErrMessage(errMessage)}
                                    setField={(fieldName, fieldValue) => setField(fieldName, fieldValue)}
                                />
                            )
                            }

                            <FinalFromWizardHealthServices
                                validate={(values) => healthServicesValidation(values)}
                                label="الخدمات الصحية"
                                editMode={editMode} />

                            <FinalFromWizardPersonsPage
                                nextFun={(values) => personsValidation(values)}
                                label="معلومات الكوادر"
                                editMode={editMode} />

                            <FinalFromWizardOtherAttachments
                                nextFun={(values) => personsValidation(values)}
                                label="مرفقات أخرى"
                                setErrMessage={(errMessage) => setErrMessage(errMessage)}
                                editMode={editMode}
                                setIsEnableNextBtn={(isEnable) => setIsEnableNextBtn(isEnable)} />

                            <FinalFromWizarTermsPage label="الإقرار والتعهد" />

                        </FinalFromWizard>
                        :
                        <CircularProgress size="15rem" style={{
                            display: 'block',
                            marginLeft: 'auto',
                            marginRight: 'auto', color: '#E2E8EB'
                        }} />
                    }
                </CardContent>
            </Card>
            <AlertDialog dialogContent={dialogContent} dialogTitle={dialogTitle} onClose={handleClose} open={open} acceptBtnName="تم" />
        </Container >
    );
}

const FinalFromWizarCenterDetailsPage = ({ validate, setIsEnableNextBtn, values, setField }) => (
    <Box>
        <NewOwnerCenterDetails
            values={values}
            setField={(fieldName, fieldValue) => setField(fieldName, fieldValue)}
            setIsEnableNextBtn={(isEnable) => setIsEnableNextBtn(isEnable)}
        />
    </Box>

);
const FinalFromWizarNewOwnerDetailsPage = ({ values, validate, setField, setIsEnableNextBtn }) => (
    <Box>
        <NewOwnerDetails
            values={values}
            setField={(fieldName, fieldValue) => setField(fieldName, fieldValue)}
            setIsEnableNextBtn={(isEnable) => setIsEnableNextBtn(isEnable)}
        />
    </Box>
);

const FinalFromWizardCRNumberAndLicensesPage = ({
    setField,
    validate,
    setErrMessage,
    editMode,
    setEditMode,
    values,
    centerLicenseNumber,
    setIsEnableNextBtn,
    fromDraft }) => (
    <>
        <CRNumberAndLicenses
            Condition={calculationConditionComp}
            values={values}
            setErrMessage={(errMessage) => setErrMessage(errMessage)}
            setField={(fieldName, fieldValue) => setField(fieldName, fieldValue)}
            editMode={editMode}
            setIsEnableNextBtn={(isEnable) => setIsEnableNextBtn(isEnable)}
            setEditMode={setEditMode}
            fromDraft={fromDraft}
        />
    </>
);

const FinalFromWizardCapacityOfNewCenterPage = ({ validate, changeLocation, formEdit, setIsEnableNextBtn, setCenterLicenseNumber, values, showSummary, isLoading, getCentertDetails, setShowSummary, renewableLicenses, setField, fromDraft }) => (
    <Box>
        <CapacityForNewCenter
            values={values}
            formEdit={formEdit}
            setField={(fieldName, fieldValue) => setField(fieldName, fieldValue)}
            setIsEnableNextBtn={(isEnable) => setIsEnableNextBtn(isEnable)}
            isLoading={isLoading}
            changeLocation={changeLocation}
            fromDraft={fromDraft}
        />
    </Box>

);

const FinalFromWizardAddressPage = ({ validate, setField, values, setIsEnableNextBtn, setErrMessage }) => (
    <Box>
        <NewCenterAddress
            values={values}
            Condition={ConditionComp}
            setField={(fieldName, fieldValue) => setField(fieldName, fieldValue)}
            setIsEnableNextBtn={(isEnable) => setIsEnableNextBtn(isEnable)}
            setErrMessage={(errMessage) => setErrMessage(errMessage)}
        />
    </Box>
);

const FinalFromWizardHealthServices = ({ editMode, setField, temporaryLicenses, values }) => (
    <>
        <HealthServices
            Condition={ConditionComp}
            values={values}
            temporaryLicenses={temporaryLicenses}
            setField={(fieldName, fieldValue) => setField(fieldName, fieldValue)}
            editMode={editMode}
        />
    </>
);
const FinalFromWizardPersonsPage = ({ editMode, label, validate, setField, pop, push, values }) => (
    <>
        <PersonDetials
            MedicalPracticeCondition={MedicalPracticeComp}
            Condition={ConditionComp}
            setField={(fieldName, fieldValue) => setField(fieldName, fieldValue)}
            pop={pop}
            push={push}
            values={values}
            editMode={editMode}
        />
    </>
);
const FinalFromWizardOtherAttachments = ({ editMode, label, validate, setField, pop, push, values, setErrMessage, setIsEnableNextBtn }) => (
    <>
        <OtherAttachment
            setErrMessage={(errMessage) => setErrMessage(errMessage)}
            setField={(fieldName, fieldValue) => setField(fieldName, fieldValue)}
            setIsEnableNextBtn={(isEnable) => setIsEnableNextBtn(isEnable)}
            editMode={editMode}
            values={values}
        />
    </>
);
const FinalFromWizarTermsPage = ({ values, setField }) => (
    <Box>
        <Terms
            values={values}
            setField={(fieldName, fieldValue) => setField(fieldName, fieldValue)}
        />
    </Box>
);

export default TransferNewOwnership;