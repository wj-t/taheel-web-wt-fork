/* eslint-disable */
import { Button, Grid } from '@material-ui/core';
import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router';
import { useLocation } from 'react-router-dom';
import AlertDialog from 'src/Core/Components/AlertDialog';
import { useLookup, useUpdateLookup } from 'src/Core/Contexts/useLookup';
import IconsList from 'src/Core/SchemaBuilder/FieldsInputs/IconsList';
import FormCreator from 'src/Core/SchemaBuilder/FormCreator';
import IconsTypeEnum from 'src/Core/SchemaBuilder/Utils/IconsTypeEnum';
import { cancelTCRequest, getRequestDetails } from "../../API/ServicesApi";
import TransferCenterNewOwnershipSchema from '../Schema/TransferCenterNewOwnershipSchema';
import TransferCenterOwnershipSchema from '../Schema/TransferCenterOwnershipSchema';
import { formateGetRequestDetails } from '../Utils/FormateJson';

const TransferCenterOwnershipSummary = () => {
    const location = useLocation()
    const navigate = useNavigate();
    const lookupValues = useLookup()
    const refreshLookup = useUpdateLookup()
    const search = location.search;
    const params = new URLSearchParams(search);
    const requestDetails = location.state?.requestDetails;
    const [licenseNumber, setLicenseNumber] = useState(location.state?.licenseNumber || params.get('licenseNumber'))
    const [taskID, setTaskID] = useState()
    const requestNum = location.state?.requestNum || params.get('requestNum');
    const [details, setDetails] = useState(false)
    const [errMessage, setErrMessage] = useState()
    const [alertComment, setAlertComment] = useState()
    const [loading, setLoading] = useState(true)
    const [open, setOpen] = useState(false)
    const [btnsOptions, setBtnsOptions] = useState({})
    const [dialogContent, setDialogContent] = useState("")
    const [dialogTitle, setDialogTitle] = useState("")
    const [newOwner, setNewOwner] = useState(false)
    const [buttonLabel, setButtonLabel] = useState()
    const [title, setTitle] = useState('تفاصيل طلب نقل ملكية مركز')

    useEffect(async () => {
        lookupValues?.isEmpity && (refreshLookup())
        setLoading(true)
        const getReqDetails = await getRequestDetails(requestNum)
        if (!getReqDetails.isSuccessful) {
            setErrMessage(getReqDetails.message)
        } else {
            let Details = getReqDetails.responseBody.requestDetails.data
            const status = Details.request?.status;
            setAlertComment({ msg: Details.request?.comment, title: 'الملاحظات' })
            setTaskID(Details?.externalTaskData?.ID)
            if (status !== 8) {
                setTitle('تفاصيل طلب نقل ملكية مركز (المالك الجديد)')
                setButtonLabel("تعديل الطلب")
                setNewOwner(true)
                const editRequestData = formateGetRequestDetails({ ...Details, requestDetails: requestDetails, status: requestDetails.status });
                Details = { editRequestData: editRequestData, ...editRequestData, center: { ...Details.center }, comment: Details.request?.comment, staff: Details?.processVariablesDump?.staff }
                console.log("Details...", Details)
            } else {
                setTitle('تفاصيل طلب نقل ملكية مركز (المالك الحالي)')
                setButtonLabel("استكمال متطلبات نقل ملكية مركز")
                setNewOwner(false)
                Details = {
                    ...Details,
                    NewCenterLocationData: { ...Details.processVariablesDump },
                    center: { ...Details.center },
                    comment: Details.request?.comment,
                    targetedBenificiray: Details.center?.targetedBeneficiary,
                    targetedServices: Details.center?.targetedServices,
                    centerType: Details.center?.type
                }
            }
            console.log("Details+++++++++++++", Details)
            setLicenseNumber(Details.center.centerLicense_r.LicenseNumber)
            setDetails(Details)
            setLoading(false)
        }
    }, [])

    async function onCancelTCRequest() {
        setLoading(true)
        const deleteCommissioner = await cancelTCRequest(taskID, licenseNumber)
        if (!deleteCommissioner.isSuccessful) {
            setErrMessage(deleteCommissioner.message);
            return { isSquccessful: false, message: deleteCommissioner.message };
        } else {
            setLoading(false)
            setBtnsOptions({
                acceptBtnName: "تم", onClose: () => {
                    navigate("/app/center-requests", {
                        state: {
                            centerLicenseNumber: licenseNumber,
                            taskID: taskID
                        }
                    })
                }
            });
            setDialogContent(`${deleteCommissioner.responseBody.data.message} ` + requestNum);
            setDialogTitle('')
            setOpen(true);

            console.log('navegate');
        }
        return { isSquccessful: true, message: "تم الإلغاء بنجاح" };
    }
    const additionalFields = () => {
        return !!taskID &&
            (
                <Grid container spacing={2} mt={3} justifyContent="space-between">
                    <Grid item>
                        <Button
                            variant="contained"
                            color="secondary"
                            onClick={() => {
                                setBtnsOptions({ onClose: () => { setOpen(false) }, buttons: { leftBtn: { title: 'نعم', func: () => { setOpen(false); onCancelTCRequest(); } }, rightBtn: { title: 'لا', func: () => { setOpen(false) } } } });
                                setDialogContent('هل أنت متأكد من إلغاء طلب نقل الملكية ؟ ');
                                setDialogTitle('إلغاء طلب نقل الملكية')
                                setOpen(true);
                            }
                            }
                        >
                            <IconsList iconType={IconsTypeEnum.DELETE_ICON} label="إلغاء الطلب" color="info" />
                        </Button>
                    </Grid>
                    <Grid item>
                        <Button
                            variant="contained"
                            color="primary"
                            sx={{
                                backgroundColor: '#3c8084',
                            }}
                            onClick={() => {
                                navigate("/center-services/transNewOnership", {
                                    state: {
                                        licenseNumber: licenseNumber,
                                        requestNum,
                                        editRequestData: details.editRequestData
                                    }
                                })
                            }}
                        >
                            <IconsList iconType={IconsTypeEnum.EDIT_ICON} label={buttonLabel} color="info" />
                        </Button>
                    </Grid>
                </Grid >
            )
    }
    return (
        <>
            <AlertDialog dialogContent={dialogContent} dialogTitle={dialogTitle} open={open} {...btnsOptions} />

            <FormCreator
                title={title}
                lookupObject={lookupValues}
                schema={newOwner ? TransferCenterNewOwnershipSchema : TransferCenterOwnershipSchema}
                errMessage={errMessage}
                initValues={details}
                alertComment={alertComment}
                isLoading={loading}
                navBackUrl={{ url: '/app/center-requests', state: { licenseNumber: licenseNumber } }}
                additionalFields={additionalFields()}
                formType='view'
            />
        </>
    )

}



export default TransferCenterOwnershipSummary;
