/* eslint-disable */
import {
  Divider,
  Grid,
  Typography
} from '@material-ui/core';
import React from 'react';
import { Field } from 'react-final-form';
import { getFieldValue } from 'src/Core/SchemaBuilder/Utils/CoreUtils';
import centerDataFieldSchema from '../Schema/centerDataFieldSchema';

const contentField = ({ input: { value, name }, label, type, inputType, values }) => {
  const val = inputType !== 'Select' && inputType !== 'Radio' ? value : getFieldValues({ name, value, values })
  return (
    !!val ? (
      <Grid
        item
        key={name + label}
        lg={6}
        md={6}
        xs={12}
      >
        <Typography gutterBottom variant="body2" color="textSecondary" component="p">
          {label}
        </Typography>
        <Typography gutterBottom variant="h5" component="h2">
          {val}
        </Typography>
      </Grid >) : null
  )
}

const getFieldValues = ({ name, value, values }) => {
  if (value == '')
    return '';
  if (!!values.lookupValues && !!values.lookupValues[name]) {
    const options = values.lookupValues[name];
    return getFieldValue({ value, options, values })
  } else {
    const filredTemp = centerDataFieldSchema?.filter(tempLicense => tempLicense.name === name)[0];
    if (!!filredTemp) {
      const filteredvalue = filredTemp?.options?.filter(option => option.value === value);
      if (Array.isArray(filteredvalue) && filteredvalue.length > 0)
        return filteredvalue[0].label.ar;
    }
  }

  return value;
}

const CenterSummary = ({ values, isOwnership, setIsEnableNextBtn }) => {

  return (
    <>
      <Typography
        color="textPrimary"
        gutterBottom
        mb={4}
        mt={6}
        variant="h4"
      >
        بيانات المركز
      </Typography>
      <Grid
        container
        spacing={3}
        mt={3}
        mb={3}
      >

        {isOwnership ? centerDataFieldSchema.filter(fintalLicense => fintalLicense.sectionName === "ownership" && !fintalLicense.dependOn).map(filteredFinalLicense => (
          <Field
            label={filteredFinalLicense.label.ar}
            name={filteredFinalLicense.name}
            values={values}
            component={contentField}
            inputType={filteredFinalLicense.type}
          />
        ))
          :
          centerDataFieldSchema.filter(fintalLicense => fintalLicense.sectionName === "CenterDetails" && !fintalLicense.dependOn).map(filteredFinalLicense => (
            <Field
              label={filteredFinalLicense.label.ar}
              name={filteredFinalLicense.name}
              values={values}
              component={contentField}
              inputType={filteredFinalLicense.type}
            />
          ))}

        {isOwnership ?
          <Grid
            item
            lg={12}
            md={12}
            xs={12}
          >
            <Typography
              color="textPrimary"
              gutterBottom
              mb={4}
              mt={6}
              variant="h4"
            >
              عنوان المركز
            </Typography>
          </Grid>
          : <Grid>
          </Grid>}
        <Divider />
        {isOwnership ? centerDataFieldSchema.filter(tempLicense => tempLicense.sectionName === "CenterAddress" && !tempLicense.dependOn).map(filteredTempLicense => (
          <Field
            label={filteredTempLicense.label.ar}
            name={filteredTempLicense.name}
            values={values}
            component={contentField}
            inputType={filteredTempLicense.type}
          />
        ))

          : <Grid>
          </Grid>}
      </Grid>
      <Divider />

    </>
  )
}

export default CenterSummary;
