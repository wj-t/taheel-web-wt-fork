/* eslint-disable */
import {
    Button, Grid
} from '@material-ui/core';
import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router';
import { useLocation } from 'react-router-dom';
import AlertDialog from 'src/Core/Components/AlertDialog';
import IconsList from 'src/Core/SchemaBuilder/FieldsInputs/IconsList';
import FormCreator from 'src/Core/SchemaBuilder/FormCreator';
import IconsTypeEnum from 'src/Core/SchemaBuilder/Utils/IconsTypeEnum';
import { cancelTCRequest, getRequestDetails } from 'src/Modules/CenterServices/API/ServicesApi';
import TransferCenterLocationSchema from '../Schema/TransferCenterLocationSchema';

const TransferCenterLocationSummary = () => {
    const location = useLocation()
    const navigate = useNavigate();
    const licenseNumber = location.state.licenseNumber
    const [taskID, setTaskID] = useState()
    const requestNum = location.state.requestNum
    console.log("TransferCenterLocationSummary :: licenseNumber: ", licenseNumber)
    console.log("TransferCenterLocationSummary :: requestNum: ", requestNum)
    console.log("TransferCenterLocationSummary :: taskID: ", taskID)
    const [details, setDetails] = useState(false)
    const [isAgree, setIsAgree] = useState(false)
    const [errMessage, setErrMessage] = useState()
    const [alertComment, setAlertComment] = useState()
    const [loading, setLoading] = useState(true)
    const [open, setOpen] = useState(false)
    const [btnsOptions, setBtnsOptions] = useState({})
    const [dialogContent, setDialogContent] = useState("")
    const [dialogTitle, setDialogTitle] = useState("")
    const handleClickOpen = (data) => {
        setErrMessage('')
        setOpen(true);
    };
    useEffect(async () => {
        setLoading(true)
        const getReqDetails = await getRequestDetails(requestNum)
        if (!getReqDetails.isSuccessful) {
            setErrMessage(getReqDetails.message)
        } else {
            let Details = getReqDetails.responseBody.requestDetails.data
            setAlertComment({ msg: Details?.request?.comment, title: 'الملاحظات' })
            setTaskID(Details?.externalTaskData?.ID)
            Details = { NewCenterLocationData: Details.processVariablesDump.NewCenterLocationData, center: Details.center, comment: Details.request?.comment }
            console.log("Details+++++++++++++", Details)
            setDetails(Details)
            setLoading(false)
        }
    }, [])
    async function onCancelTCRequest() {
        setLoading(true)
        const deleteCommissioner = await cancelTCRequest(taskID, licenseNumber)
        if (!deleteCommissioner.isSuccessful) {
            setErrMessage(deleteCommissioner.message);
            return { isSquccessful: false, message: deleteCommissioner.message };
        } else {
            setLoading(false)
            setBtnsOptions({
                acceptBtnName: "تم", onClose: () => {
                    navigate("/app/center-requests", {
                        state: {
                            centerLicenseNumber: licenseNumber,
                            taskID: taskID
                        }
                    })
                }
            });
            setDialogContent(`${deleteCommissioner.responseBody.data.message} ` + requestNum);
            setDialogTitle('')
            setOpen(true);

            console.log('navegate');
        }
        return { isSquccessful: true, message: "تم الحذف بنجاح" };
    }
    const title = 'تفاصيل طلب نقل المركز'
    const additionalFields = () => {
        return !!taskID &&
            (
                <Grid container spacing={2} mt={3} justifyContent="space-between">
                    <Grid item>
                        <Button
                            variant="contained"
                            color="secondary"
                            onClick={() => {
                                setBtnsOptions({ onClose: () => { setOpen(false) }, buttons: { leftBtn: { title: 'نعم', func: () => { setOpen(false); onCancelTCRequest(); } }, rightBtn: { title: 'لا', func: () => { setOpen(false) } } } });
                                setDialogContent('هل أنت متأكد من إلغاء طلب نقل المركز ؟ ');
                                setDialogTitle('إلغاء طلب نقل المركز')
                                setOpen(true);
                            }
                            }
                        >
                            <IconsList iconType={IconsTypeEnum.DELETE_ICON} label="إلغاء الطلب" color="info" />


                        </Button>
                    </Grid>
                    <Grid item>
                        <Button
                            variant="contained"
                            color="primary"
                            sx={{
                                backgroundColor: '#3c8084',
                            }}
                            onClick={() => {
                                navigate("/center-services/transfercenter", {
                                    state: {
                                        licenseNumber: licenseNumber,
                                        centerLicenseNumber: licenseNumber,
                                        taskID,
                                        requestNum,
                                        formEdit: true
                                    }
                                })
                            }}
                        >
                            <IconsList iconType={IconsTypeEnum.EDIT_ICON} label="تعديل بيانات طلب نقل المركز" color="info" />
                        </Button>
                    </Grid>
                </Grid >
            )
    }
    return (
        <>
            <AlertDialog dialogContent={dialogContent} dialogTitle={dialogTitle} open={open} {...btnsOptions} />
            <FormCreator
                title={title}
                schema={TransferCenterLocationSchema}
                errMessage={errMessage}
                initValues={details}
                alertComment={alertComment}
                additionalFields={additionalFields()}
                isLoading={loading}
                navBackUrl={{ url: '/app/center-requests', state: { licenseNumber: licenseNumber } }}
                formType='view'
            />
        </>
    )
}
TransferCenterLocationSummary.propTypes = {
    // centers: PropTypes.array.isRequired
}

export default TransferCenterLocationSummary
