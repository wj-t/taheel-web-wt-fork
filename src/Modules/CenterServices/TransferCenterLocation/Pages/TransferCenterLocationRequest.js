/* eslint-disable */
import React, { useEffect, useState } from 'react';
import {
    Alert,
    AlertTitle, Box,
    Card,
    CardContent,
    CardHeader, CircularProgress, Container, Divider
} from '@material-ui/core';
import FinalLicenseData from '../Sections/FinalLicenseData';
import NewCenterAddress from '../Sections/NewCenterAddress';
import NewLocationData from '../Sections/NewLocationData';
import Terms from '../Sections/Terms';
import DraftsTwoToneIcon from '@material-ui/icons/DraftsTwoTone';
import numeral from 'numeral';
import { useLocation, useNavigate } from 'react-router-dom';
import AlertDialog from 'src/Core/Components/AlertDialog';
import FinalFromWizard from 'src/Core/Components/wizard/FinalFormWizard';
import { LICENSE_FORM_TYPES } from 'src/Core/Utils/enums';
import { getCurrentUser } from 'src/Core/Utils/UserLocalStorage';
import { dateFormatter, reverseRange } from 'src/Core/Utils/utilFunctions';
import { CentertDetails, getCentersForFinalNoExpired, getRequestDetails } from 'src/Modules/CenterServices/API/ServicesApi';
import { CenterDetailsValidation, centerTypeJSON, getStaff } from '../../FinalLicense/Utils/finalLicenseUtil';
import { ConditionComp } from '../../TemporaryLicense/Utils/temporayLicenseUtil';
import { getAddressFromObject } from '../../TransferCenterOwnership/Utils/FormateJson';
import { centerLocationTransferAPIFunc } from '../API/TransferCenterLocationAPI';
import { AttachementValidation, NewAddressValidation } from '../Utils/TransferCenterLoactionUtil';
import { extractDateToObject, getDocId } from 'src/Core/Utils/TaheelUtils'

const TransferCenterLocationRequest = () => {
    const location = useLocation();
    const navigate = useNavigate();
    const [renewableLicenses, setRenewableLicenses] = useState([]);
    const [errMessage, setErrMessage] = useState('');
    const [dialogContent, setDialogContent] = useState("");
    const [dialogTitle, setDialogTitle] = useState("");
    const [open, setOpen] = useState(false);
    const [isEnableNextBtn, setIsEnableNextBtn] = useState(false);
    const [isLoading, setIsLoading] = useState(true);
    const [editInitValues, setEditInitValues] = useState({});
    const [editMode, setEditMode] = useState(false);
    const [centerLicenseNumber, setCenterLicenseNumber] = useState(location.state ? location.state.licenseNumber : "1");
    const requestNum = location.state?.requestNum;
    const taskID = location.state?.taskID;
    const formEdit = location.state?.formEdit
    const fromDraft = location.state?.fromDraft
    const [showSummary, setShowSummary] = useState(false);
    const formType = location.state ? location.state.formType : null;
    const [center, setCenter] = useState({});
    const [formInits, setFormInits] = useState({});
    const { email } = getCurrentUser();

    useEffect(async () => {
        if (!!formEdit || fromDraft) {
            setIsLoading(true);
            setRenewableLicenses([{ licenseNumber: centerLicenseNumber }]);
            await getCentertDetails(centerLicenseNumber);

            setIsLoading(true);

            const getReqDetails = await getRequestDetails(requestNum)
            if (!getReqDetails.isSuccessful) {
                setErrMessage(getReqDetails.message)
            } else {
                let Details = getReqDetails.responseBody.requestDetails.data
                if (Details.draft_values.isDraft) {
                    setFormInits({
                        buildingArea: Details.draft_values?.draft_values?.buildingArea,
                        basementArea: Details.draft_values?.draft_values?.basementArea,
                        buildNo: Details.draft_values?.draft_values?.buildNo,
                        capacity: null,
                        ...extractDateToObject(Details.draft_values?.center?.centerInfo_r?.expirarionDateForFireDepartmentLicenseHijri),
                        city: Details.draft_values?.draft_values?.city,
                        buildNo: Details.draft_values?.draft_values?.buildNo,
                        street: Details.draft_values?.draft_values?.street,
                        sub: Details.draft_values?.draft_values?.sub,
                        postalCode: Details.draft_values?.draft_values?.postalCode,
                        additionalNo: Details.draft_values?.draft_values?.additionalNo,
                        Furniture: getDocId(Details.draft_values?.draft_values?.Furniture),
                        municipLicenseNo: getDocId(Details.draft_values?.draft_values?.municipLicenseNo),
                        fireDepartmentLicense: getDocId(Details.draft_values?.draft_values?.fireDepartmentLicense),
                        OfficeReport: getDocId(Details.draft_values?.draft_values?.OfficeReport),
                        address: getAddressFromObject(Details.draft_values?.draft_values),
                    })
                }
                else {
                    Details = { NewCenterLocationData: { ...Details.processVariablesDump.NewCenterLocationData }, center: { ...Details.center } }
                    const date = Details?.NewCenterLocationData?.centerInfo_r?.expirarionDateForFireDepartmentLicenseHijri;
                    setFormInits({
                        buildingArea: Details?.NewCenterLocationData?.centerInfo_r?.buildingArea,
                        basementArea: Details?.NewCenterLocationData?.centerInfo_r?.basementArea,
                        buildNo: Details?.NewCenterLocationData?.centerLocation_r?.buildNo,
                        capacity: null,
                        ...extractDateToObject(date),
                        city: Details?.NewCenterLocationData?.centerLocation_r?.city,
                        buildNo: Details?.NewCenterLocationData?.centerLocation_r?.buildNo,
                        street: Details?.NewCenterLocationData?.centerLocation_r?.street,
                        sub: Details?.NewCenterLocationData?.centerLocation_r?.area,
                        postalCode: Details?.NewCenterLocationData?.centerLocation_r?.postalCode,
                        additionalNo: Details?.NewCenterLocationData?.centerLocation_r?.additionalNo,
                        // OfficeReport: [center && center.centerInfo_r && center.centerInfo_r.engineeringPlan && (center.centerInfo_r.engineeringPlan || center.centerInfo_r.engineeringPlan.id)],
                        Furniture: [Details?.NewCenterLocationData?.centerInfo_r?.furniturePhoto_r.map(f => f.Document)],
                        municipLicenseNo: [Details?.NewCenterLocationData?.centerInfo_r?.momraDoc],
                        fireDepartmentLicense: [Details?.NewCenterLocationData?.centerInfo_r?.fireDepartmentLicense],
                        OfficeReport: [Details?.NewCenterLocationData?.centerInfo_r?.engineeringPlan],
                        taskID: taskID,
                    })
                }
                setIsEnableNextBtn(true)
                setIsLoading(false);
            }
        } else {
            setIsLoading(true);
            const getCentersRs = await getCentersForFinalNoExpired(email);

            setErrMessage("");
            if (!getCentersRs.isSuccessful) {
                setErrMessage(getCentersRs.message);
                setIsLoading(false);
            } else {
                const Centers = getCentersRs.responseBody.data?.Centers?.map(c => { return { ...c, centerLicenseNumber: c.centerLicense_r.LicenseNumber } });
                setRenewableLicenses(Centers);
                setIsLoading(false);
            }
        }
    }, [])

    const getCentertDetails = async (licenseNumber) => {
        setIsLoading(true)
        setErrMessage("");
        const response = await CentertDetails(licenseNumber)
        if (response?.responseBody?.data?.center) {
            const attach = response.responseBody.data.center && response.responseBody.data.center.centerInfo_r && response.responseBody.data.center.centerInfo_r.operationPlan && response.responseBody.data.center.centerInfo_r.operationPlan.id;
            const crNum = response.responseBody.data.center.crInfo_r.crNumber;
        }
        if (!response.isSuccessful) {
            setErrMessage(response.message);
            setIsLoading(false);
        }
        else {
            setEditInitValues(response?.responseBody?.data);
            setCenter(response?.responseBody?.data?.center)
            setCenterLicenseNumber(licenseNumber)
            setEditMode(true);
            setIsLoading(false);
            setShowSummary(true);
            return response.responseBody.data;
        }
    }
    const handleClickOpen = (dialogContent, dialogTitle) => {
        setDialogContent(dialogContent);
        setDialogTitle(dialogTitle)
        setOpen(true);
    };

    const handleClose = (value) => {
        setOpen(false);
        navigate('/app/dashboard', { replace: true });
    };

    const onSubmit = async (values) => {
        setIsLoading(true);

        const response = await centerLocationTransferAPIFunc(values);
        if (response.isSuccessful) {
            if (values.isDraft && !!response?.responseBody?.data) {
                handleClickOpen(`${response.responseBody.data.message[0]} طلب رقم ${response.responseBody.data.requestNumber}`, '');
            }
            else {
                handleClickOpen(`${response.responseBody.data.message}`, '');
            }
        }
        else {
            setErrMessage(`${response.message}`);
            setIsLoading(false)
        }
    };

    return (
        <Container maxWidth="md">
            <Card>
                <CardHeader
                    title="نقل مقر مركز أهلي"
                />
                <Divider />
                {!isLoading && fromDraft &&
                    <Alert icon={<DraftsTwoToneIcon sx={{ color: 'grey !important' }} />} variant="outlined" severity="info" sx={{ marginLeft: 2, marginRight: 2, marginTop: 1, color: 'grey !important', borderColor: 'grey !important' }}>
                        <AlertTitle> مسودة رقم {requestNum}</AlertTitle>
                        {editInitValues?.request && editInitValues.request?.comment}
                    </Alert>
                }
                {errMessage && (
                    <Alert variant="outlined" severity="error">
                        {errMessage}
                    </Alert>
                )}
                <CardContent>
                    {!isLoading ?
                        <FinalFromWizard
                            initialValues={{
                                agree: [],
                                isNextBtnDisabled: false,
                                managersCount: 0,
                                teachersCount: 0,
                                centerType: center && center.type && center.targetedBeneficiary && center.targetedServices
                                    && centerTypeJSON.type[parseInt(center.type)] && centerTypeJSON.targetedBeneficiary[parseInt(center.targetedBeneficiary)] && centerTypeJSON.targetedBeneficiary[parseInt(center.targetedBeneficiary)] && centerTypeJSON.targetedServices[parseInt(center.targetedServices)]
                                    && centerTypeJSON.type[parseInt(center.type)].name + ' - ' + centerTypeJSON.targetedBeneficiary[parseInt(center.targetedBeneficiary)].name + ' - ' + centerTypeJSON.targetedServices[parseInt(center.targetedServices)].name,
                                CRNumber: center && center.crInfo_r && center.crInfo_r.crNumber,
                                centerLicenseNumber: center?.centerLicense_r?.LicenseNumber,
                                temporaryLicenseNum: centerLicenseNumber,
                                licenseCreationDate: center && dateFormatter(center.centerLicense_r?.creationDate),
                                licenseExpiryDate: center && dateFormatter(center.centerLicense_r?.expirationDate),
                                ownerName: center && center.centerOwner_r?.ownerName,
                                ownerID: center && center.centerOwner_r?.ownerID,
                                centerAgeGroup: center && center.ageGroup && reverseRange(center.ageGroup),
                                centerGenderGroup: center
                                    && center.targetedGender &&
                                    (center.targetedGender === "m" ? "ذكر" : (center.targetedGender === "f" ? "أنثى" : "كلا الجنسين")),
                                CRNumber: center && center.crInfo_r && center.crInfo_r.crNumber,
                                companyName: center && center.crInfo_r && center.crInfo_r.entityName,
                                activities: center && center.crInfo_r && center.crInfo_r.crActivityType,
                                // municipLicenseNo: center && center.crInfo_r && center.crInfo_r.MoMRA_License,
                                newCapacity: center && center.centerInfo_r && numeral(center.centerInfo_r.carryingnumber).format('0,0'),
                                financialGuarantee: center && center.centerInfo_r && `${numeral(center.centerInfo_r.financialGuarantee).format('0,0.00')} ر.س.`,
                                beneficiariesNum: center && center.centerInfo_r && center.centerInfo_r.beneficiaryCount,
                                //getting the form initial values if exist
                                ...formInits,
                                requestNum: requestNum,
                                isDraft: false,

                                SecurityReport: center && center.centerInfo_r && [center.centerInfo_r.securityReport && (center.centerInfo_r.securityReport || center.centerInfo_r.securityReport.id)],
                                OperationalPlan: [center && center.centerInfo_r && center.centerInfo_r.operationPlan && (center.centerInfo_r.operationPlan || center.centerInfo_r.operationPlan.id)],
                                ExecutivePlan: [center && center.centerInfo_r && center.centerInfo_r.executivePlan && (center.centerInfo_r.executivePlan || center.centerInfo_r.executivePlan.id)],

                                // Furniture: center && center.centerInfo_r && center.centerInfo_r.furniturePhoto_r && (center.centerInfo_r.furniturePhoto_r.map(d => d.Document) || center.centerInfo_r.furniturePhoto_r.map(d => d.Document.id)),
                                FinancialGuaranteeAtt: [center && center.centerInfo_r && center.centerInfo_r.financialGuarbteeAtt && (center.centerInfo_r.financialGuarbteeAtt || center.centerInfo_r.financialGuarbteeAtt.id)],
                                healthServices: center && center.centerInfo_r && center.isHealthCareServices ? "yes" : "no",
                                healthServiceType: center && center.centerInfo_r && center.healthCareServices_r && center.healthCareServices_r.type,
                                // healthServiceAttachment: center.centerInfo_r.financialGuarbteeAtt,
                                healthServiceAttachment: [center && center.centerInfo_r && center.healthCareServices_r && center.healthCareServices_r.attachment && (center.healthCareServices_r.attachment || center.healthCareServices_r.attachment.id)],
                                customers: editInitValues?.staff && getStaff(editInitValues?.staff),
                                page: formType === LICENSE_FORM_TYPES.RENEW ? 1 : 0,
                                formType: formType

                            }}
                            cancelBtnFn={() => { navigate('/app/center-services-list', { replace: true }); }}
                            isEnableCancelBtn={centerLicenseNumber === '1'}
                            isEnableEndBtn={centerLicenseNumber !== '1'}
                            isEnableNextBtn={isEnableNextBtn}
                            requestNum={requestNum}
                            email={email}
                            showSummary={showSummary}
                            onSubmit={onSubmit}
                        >
                            <FinalFromWizardLicenseDataPage label="بيانات الترخيص النهائي "
                                formEdit={!!formEdit}
                                validate={CenterDetailsValidation}
                                renewableLicenses={renewableLicenses}
                                setCenterLicenseNumber={setCenterLicenseNumber}
                                showSummary={showSummary}
                                setShowSummary={setShowSummary}
                                getCentertDetails={getCentertDetails}
                                setIsEnableNextBtn={(isEnable) => setIsEnableNextBtn(isEnable)}
                                fromDraft={fromDraft}
                            />
                            <FinalFromWizarLocationDataPage
                                label="بيانات المقر الجديد للمركز "
                                validate={(values) => AttachementValidation(values)}
                                setIsEnableNextBtn={(isEnable) => setIsEnableNextBtn(isEnable)}
                            />

                            <FinalFromWizardAddressPage
                                label="تعبئة بيانات العنوان الوطني "
                                validate={(values) => NewAddressValidation(values)}
                                setIsEnableNextBtn={(isEnable) => setIsEnableNextBtn(isEnable)}
                                setErrMessage={(errMessage) => setErrMessage(errMessage)}
                            />
                            <FinalFromWizarTermsPage
                                label="الإقرار والتعهد"
                                setIsEnableNextBtn={(isEnable) => setIsEnableNextBtn(isEnable)}
                            />
                        </FinalFromWizard>
                        :
                        <CircularProgress size="15rem" style={{
                            display: 'block',
                            marginLeft: 'auto',
                            marginRight: 'auto', color: '#E2E8EB'
                        }} />
                    }
                </CardContent>
            </Card>
            <AlertDialog dialogContent={dialogContent} dialogTitle={dialogTitle} open={open} onClose={handleClose} acceptBtnName="تم" />

        </Container>
    );
}

const FinalFromWizardLicenseDataPage = ({ validate, formEdit, setIsEnableNextBtn, setCenterLicenseNumber, values, showSummary, isLoading, getCentertDetails, setShowSummary, renewableLicenses, setField, fromDraft }) => (
    <Box>
        <FinalLicenseData
            values={values}
            formEdit={formEdit}
            setField={(fieldName, fieldValue) => setField(fieldName, fieldValue)}
            validate={CenterDetailsValidation}
            renewableLicenses={renewableLicenses}
            setCenterLicenseNumber={(centerLicenseNumber) => setCenterLicenseNumber(centerLicenseNumber)}
            showSummary={showSummary}
            setShowSummary={setShowSummary}
            setIsEnableNextBtn={(isEnable) => setIsEnableNextBtn(isEnable)}
            isLoading={isLoading}
            getCentertDetails={getCentertDetails}
            fromDraft={fromDraft}
        />
    </Box>

);

const FinalFromWizarLocationDataPage = ({ values, validate, setField, setIsEnableNextBtn }) => (
    <Box>
        <NewLocationData
            values={values}
            setField={(fieldName, fieldValue) => setField(fieldName, fieldValue)}
            setIsEnableNextBtn={(isEnable) => setIsEnableNextBtn(isEnable)}
        />
    </Box>
);

const FinalFromWizardAddressPage = ({ validate, values, setField, setIsEnableNextBtn, setErrMessage }) => (
    <Box>
        <NewCenterAddress
            Condition={ConditionComp}
            values={values}
            setField={(fieldName, fieldValue) => setField(fieldName, fieldValue)}
            setIsEnableNextBtn={(isEnable) => setIsEnableNextBtn(isEnable)}
            setErrMessage={(errMessage) => setErrMessage(errMessage)}
        />
    </Box>
);

const FinalFromWizarTermsPage = ({ values, validate, setField, setIsEnableNextBtn }) => (
    <Box>
        <Terms
            values={values}
            setField={(fieldName, fieldValue) => setField(fieldName, fieldValue)}
            setIsEnableNextBtn={(isEnable) => setIsEnableNextBtn(isEnable)}
        />
    </Box>

);

export default TransferCenterLocationRequest;

