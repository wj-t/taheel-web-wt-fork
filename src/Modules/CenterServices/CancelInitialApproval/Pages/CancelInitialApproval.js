
/* eslint-disable */
import {
  CircularProgress,
  Card,
  CardContent,
  CardHeader,
  Divider,
  Container,
  Alert,
  Grid,
  MenuItem,
} from '@material-ui/core';
import { useNavigate, useLocation } from 'react-router-dom';
import { useEffect, useState } from 'react';
import AlertDialog from 'src/Core/Components/AlertDialog';
import { getCurrentUser } from 'src/Core/Utils/UserLocalStorage';
import { CentertDetails } from '../../API/ServicesApi';
import { getTempLicense } from '../../FinalLicense/API/finalLicenseAPI';
import { Field } from 'react-final-form';
import { TextField as TextFieldFinal, Select } from 'final-form-material-ui';
import { OnChange } from 'react-final-form-listeners';
import TempFormSummary from '../Sections/TempFormSummary';
import ApprovalSummary from '../Sections/ApprovalSummary';
import { dateFormatter } from 'src/Core/Utils/utilFunctions';
import CancelReasonpDialog from '../Sections/CancelReasonpDialog';
import { useLookup, useUpdateLookup } from 'src/Core/Contexts/useLookup';
import { OWNER_TYPE } from 'src/Core/Utils/enums';

const CancelInitialApproval = () => {
  const location = useLocation();
  const navigate = useNavigate();
  const lookupValues = useLookup()
  const refreshLookup = useUpdateLookup()
  const [tempLicenses, setTempLicenses] = useState([]);
  const [errMessage, setErrMessage] = useState('');
  const [open, setOpen] = useState(false);
  const [isOpen, setIsOpen] = useState(false);
  const [isEnableNextBtn, setIsEnableNextBtn] = useState(false);
  const [isLoading, setIsLoading] = useState(true);
  const [details, setDetails] = useState({});
  const [showSummary, setShowSummary] = useState(false);
  const [approvalNum, setApprovalNum] = useState('');
  const [center, setCenter] = useState({});

  const [val, setVal] = useState([]);
  const { email, idNumIqamaNum, DOB, phoneNumber, firstName, secondName, lastName } = getCurrentUser();

  useEffect(async () => {
    lookupValues?.isEmpity && (refreshLookup())
    setIsLoading(true);
    const getCentersRs = await getTempLicense(email);
    setErrMessage("");
    if (!getCentersRs.isSuccessful) {
      setErrMessage(getCentersRs.message);
      setIsLoading(false);
    } else {
      const { Centers } = getCentersRs.responseBody.data;
      setTempLicenses(Centers);
      setIsLoading(false);
    }
  }, [])

  const getCentertDetails = async (licenceNumber) => {
    setIsLoading(true)
    setErrMessage("");
    const response = await CentertDetails(licenceNumber)
    console.log("===> getCentertDetails response: " + JSON.stringify(response))
    /*  if (response?.responseBody?.data?.center) {
       const attach = response.responseBody.data.center && response.responseBody.data.center.centerInfo_r && response.responseBody.data.center.centerInfo_r.operationPlan && response.responseBody.data.center.centerInfo_r.operationPlan.id;
       console.log('===> attach: ' + JSON.stringify(attach))
     } */
    // new api to get the draft request numbe if exist 
    // setRequestNum(value)
    if (!response.isSuccessful) {
      setErrMessage(response.message);
    }
    else {
      const centerOwner = response.responseBody?.data?.center?.centerOwner_r
      const center = response.responseBody?.data?.center
      setDetails([]);
      setDetails(details => {

        if (centerOwner.ownerType === OWNER_TYPE.LEGAL_TYPE) {
          details.commissionerMobNum = centerOwner?.ownerPhoneNumber
          details.entityName = centerOwner?.ownerName
          details.CRNumber = centerOwner?.ownerID
          details.requestType = OWNER_TYPE.LEGAL_TYPE
          delete centerOwner?.ownerName
          delete centerOwner?.ownerID
          delete centerOwner?.ownerPhoneNumber
          delete details.mobileNo
          delete details?.birthDate
        }
        if (centerOwner.ownerType === OWNER_TYPE.NATURAL_TYPE) {
          details.birthDate = DOB
          details.ownerName = center?.centerOwner_r?.ownerName
          details.ownerID = center?.centerOwner_r?.ownerID
          details.mobileNo = center?.centerOwner_r?.ownerPhoneNumber
          details.requestType = OWNER_TYPE.NATURAL_TYPE
        }
        return details
      });
      setCenter(response.responseBody.data.center)
      setIsLoading(false);
      setShowSummary(true);
      // return response.responseBody.data;
      return;
    }
  }
  const handleClickOpen = () => {
    setErrMessage("");
    setOpen(true);
  };
  const handleClose = () => {
    setErrMessage("");
    setOpen(false);
  };
  const handleIsClose = () => {
    setErrMessage("");
    setIsOpen(false);
    navigate('/app/center-requests', { replace: true });
  };
  const onSubmit = async (values) => {
    setErrMessage("")
    setVal(values);
    handleClickOpen();
  }
  return (
    <Container maxWidth="md">
      <Card>
        <CardHeader
          title="طلب إلغاء موافقة مبدئية"
        />
        <Divider />
        {errMessage && (
          <Alert variant="outlined" severity="error">
            {errMessage}
          </Alert>
        )}
        <CardContent>
          {!isLoading ?
            <>
              <TempFormSummary
                initialValues={{
                  agree: [false],
                  beneficiariesNum: 0,
                  centerLicenceNumber: center?.centerLicense_r?.LicenseNumber,
                  centerName: center && center.name,
                  centerType: center?.type,
                  centerName: center && center.name,
                  licenseExpiryDate: center?.centerLicense_r?.expirationHijri,
                  targetedServices: center?.targetedServices,
                  city: center?.centerLocation_r?.city || center?.city,
                  buildNo: center?.centerLocation_r?.buildNo || center?.buildNo,
                  street: center?.centerLocation_r?.street || center?.street,
                  sub: center?.centerLocation_r?.area || center?.area,
                  postalCode: center?.centerLocation_r?.postalCode || center?.postalCode,
                  additionalNo: center?.centerLocation_r?.additionalNo || center?.additionalNo,
                  targetedGender: center?.targetedGender,
                  questionnairesScore: center?.questionnairesScore,
                  lookupValues: lookupValues,
                  targetedBenificiray: center?.targetedBeneficiary,
                  targetedServices: center?.targetedServices,
                  centerType: center?.type,
                  workingHours: center?.workingHours,
                  ...details
                }}
                cancelBtnFn={() => { navigate('/app/center-services-list', { replace: true }); }}
                isEnableCancelBtn={true}
                isEnableNextBtn={isEnableNextBtn}
                showSummary={showSummary}
                onSubmit={onSubmit}
              >
                <FinalFormTempSummary
                  tempLicenses={tempLicenses}
                  setApprovalNum={setApprovalNum}
                  showSummary={showSummary}
                  setShowSummary={setShowSummary}
                  getCentertDetails={getCentertDetails} />
              </TempFormSummary>
            </>
            :
            <CircularProgress size="15rem" style={{
              display: 'block',
              marginLeft: 'auto',
              marginRight: 'auto', color: '#E2E8EB'
            }} />
          }
        </CardContent>
        <CancelReasonpDialog errMessage={errMessage} setErrMessage={setErrMessage} approvalNum={approvalNum} open={open} setOpen={(open) => handleIsClose()} onClose={handleClose} licenceNumber={center?.centerLicense_r?.LicenseNumber} />
      </Card>

    </Container>
  );
}

const FinalFormTempSummary = ({ setField, tempLicenses, values, setApprovalNum, getCentertDetails, showSummary, setShowSummary }) => {

  return (
    <>
      <Grid
        container
        mt={4}
        spacing={3}
      >
        <Grid
          item
          md={6}
          xs={12}
          className="custom-label-field"
        >
          <Field
            fullWidth
            label="رقم الموافقة المبدئية"
            name="centerLicenceNumber"
            component={Select}
            required
            dir="rtl"
            variant="outlined"
            className="custom-field"
            formControlProps={{ fullWidth: true }}
          >
            <MenuItem value="1" key="1" selected={true}>اختيار</MenuItem>
            {console.log("tempLicensestempLicenses", tempLicenses),
              tempLicenses.map(item => (
                (item.type !== "01") &&
                <MenuItem key={item.centerLicense_r.LicenseNumber} value={item.centerLicense_r.LicenseNumber}>{item.centerLicense_r.LicenseNumber}</MenuItem>
              ))}
          </Field>
          <OnChange name="centerLicenceNumber">
            {async (value) => {
              console.log(`++++++centerLicenceNumber + ${value}`);
              if (value != 1) {

                setApprovalNum(value);
                await getCentertDetails(value);
              }
              else {
                setShowSummary(false);
              }
            }}
          </OnChange>
        </Grid>
        <Grid
          item
          md={6}
          xs={12}
          className="custom-label-field"
        >
        </Grid>
      </Grid>
      {showSummary && <ApprovalSummary
        values={values}
        setField={(fieldName, fieldValue) => setField(fieldName, fieldValue)}
      />
      }
    </>
  )
}
export default CancelInitialApproval;