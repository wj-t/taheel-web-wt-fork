import { checkEmailPattern } from "src/Core/Utils/inputValidator";

const required = 'هذا الحقل مطلوب'
const cancelingReasonMissing = 'الرجاء إدخال سبب إلغاء الموافقة المبدئية'

const approvalNumValidate = values => {
  var msg = {}
  if (!values.cancelingReason)
    msg.cancelingReason = cancelingReasonMissing;
  return msg
}
export { approvalNumValidate }