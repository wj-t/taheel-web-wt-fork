import { Helmet } from 'react-helmet';
import {
  Box,
  Container,
  Grid,
  Pagination
} from '@material-ui/core';
import ServicesToolbar from 'src/Modules/CenterServices/ServicesList/Components/ServiceToolbar';
import ServiceCard from 'src/Modules/CenterServices/ServicesList/Components/ServiceCard';
import Services from 'src/__mocks__/services';

const ServicesList = () => (
  <>
    <Helmet>
      <title>Center Services</title>
    </Helmet>
    <Box
      sx={{
        backgroundColor: '#f4f6f8',
        minHeight: '100%',
        overflow: 'hidden',
      }}
    >
      <Container maxWidth={false}>
        <ServicesToolbar />
        <Box sx={{ pt: 3, }}>
          <Grid
            container
            spacing={10}
            dir="rtl"
          >
            {Services.map((Service) => (
              <Grid
                item
                key={Service.id}
                lg={4}
                md={6}
                xs={12}
              >
                <ServiceCard service={Service} />
              </Grid>
            ))}
          </Grid>
        </Box>
        <Box
          sx={{
            display: 'flex',
            justifyContent: 'center',
            pt: 3
          }}
        >
          {Services.length > 9 ? (<Pagination
            color="primary"
            count={3}
            size="small"
          />) : ''}
        </Box>
      </Container>
    </Box>
  </>
);

export default ServicesList;
