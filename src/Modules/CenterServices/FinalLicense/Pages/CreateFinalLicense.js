/* eslint-disable */
import {
  Alert, AlertTitle, Box,
  Card,
  CardContent,
  CardHeader, CircularProgress, Container, Divider
} from '@material-ui/core';
import DraftsTwoToneIcon from '@material-ui/icons/DraftsTwoTone';
import numeral from 'numeral';
import React, { useEffect, useState } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import AlertDialog from 'src/Core/Components/AlertDialog';
import FinalFromWizard from 'src/Core/Components/wizard/FinalFormWizard';
import { useLookup, useUpdateLookup } from 'src/Core/Contexts/useLookup';
import { LICENSE_FORM_TYPES, OWNER_TYPE } from 'src/Core/Utils/enums';
import { getCurrentUser } from 'src/Core/Utils/UserLocalStorage';
import { dateFormatter, reverseRange } from 'src/Core/Utils/utilFunctions';
import { CentertDetails } from 'src/Modules/CenterServices/API/ServicesApi';
import CenterDetails from 'src/Modules/CenterServices/FinalLicense/Sections/CenterDetails';
import CenterManagerInfo from 'src/Modules/CenterServices/FinalLicense/Sections/CenterManagerInfo';
import { getLookups } from '../../TemporaryLicense/API/temporayLicenseAPI';
import CenterAddress from '../../TemporaryLicense/Sections/CenterAddress';
import { NewAddressValidation } from '../../TransferCenterLocation/Utils/TransferCenterLoactionUtil';
import { getAddressFromObject } from '../../TransferCenterOwnership/Utils/FormateJson';
import { DraftDetails, getTempLicense, TaskDetails, updateFinalLicenseAPIFunc } from '../API/finalLicenseAPI';
import Capacity from '../Sections/Capacity';
import HealthServices from '../Sections/HealthServices';
import RenewalSummary from '../Sections/RenewalSummary';
import Requirements from '../Sections/Requirements';
import PersonDetials from '../Sections/staff/PersonDetials';
import Summary from '../Sections/Summary';
import { calculationConditionComp, capacityValidation, CenterDetailsValidation, CenterMangerInfoValidation, centerTypeJSON, ConditionComp, getStaff, healthServicesValidation, MedicalPracticeComp, personsValidation, RequirementsValidation } from '../Utils/finalLicenseUtil';


const CreateFinalLicense = () => {
  const navigate = useNavigate()
  const location = useLocation()
  const lookupValues = useLookup()
  const refreshLookup = useUpdateLookup()
  const [temporaryLicenses, setTemporaryLicenses] = useState()
  const [open, setOpen] = useState(false);
  const [isEnableNextBtn, setIsEnableNextBtn] = useState(false)
  const [dialogContent, setDialogContent] = useState("")
  const [dialogTitle, setDialogTitle] = useState("")
  const [centerLicenseNumber, setCenterLicenseNumber] = useState(location.state ? location.state.centerLicenseNumber : null)
  const [editMode, setEditMode] = useState(false)
  const [center, setCenter] = useState({})
  const [editInitValues, setEditInitValues] = useState({})
  const [initValues, setInitValues] = useState(location?.state?.centerData)
  const [draftValues, setDraftValues] = useState({})
  const [isLoading, setIsLoading] = useState(true)
  const [canShowSection, setCanShowSection] = useState(true)
  const [errMessage, setErrMessage] = useState('')
  const [staffTypes, setStaffTypes] = useState([])
  const [centersForDisabilities, setCentersForDisabilities] = useState(initValues?.type === "01")
  const [title, setTitle] = useState("إصدار ترخيص")
  const formType = location.state ? location.state.formType : null
  const taskID = location.state ? location.state.taskID : null
  const requestNum = location.state ? location.state.requestNum : ""
  const fromDraft = location.state ? location.state.fromDraft : false
  const { email } = getCurrentUser();

  useEffect(async () => {

    lookupValues?.isEmpity && (refreshLookup())
    setErrMessage("");
    setIsLoading(true);
    if (!!initValues) {
      if (initValues.type === "08") {
        setTitle("إصدار ترخيص - مراكز ضيافة الأطفال")
      }
      if (initValues.type === "05") {
        setTitle("إصدار ترخيص - مراكز الإرشاد الأسري")
      }
      if (initValues.type === "03") {
        setTitle("إصدار ترخيص - مراكز كبار السن ")
      }
    }

    console.log("===> formType: " + formType)
    console.log("===> centerLicenseNumber: " + centerLicenseNumber)
    console.log("===> requestNum: " + requestNum)
    console.log("===> fromDraft: " + fromDraft)
    const { email } = await getCurrentUser();
    if (!!initValues) {
      const { DOB, gender } = getCurrentUser();

      setInitValues(init => {
        if (init?.centerOwner_r?.ownerType === OWNER_TYPE.LEGAL_TYPE) {
          init.commissionerMobNum = init.centerOwner_r.ownerPhoneNumber
          init.entityName = init.centerOwner_r.ownerName
          delete init.centerOwner_r.ownerName
          delete init.centerOwner_r.ownerPhoneNumber
        } else {
          if (!init.fullName && !init.idNumber && !init.birthDate && !init.gender) {
            init.fullName = init?.centerOwner_r?.ownerName
            init.idNumber = init?.centerOwner_r?.ownerID
            init.birthDate = DOB
            init.gender = gender === 'm' ? "ذكر" : "انثى"
          }
        }
        init = {
          centerLicenseNumber: init?.centerLicense_r.LicenseNumber,
          centerAgeGroup: init?.ageGroup,
          inHouseHspit: init.type === "08" && init.targetedBeneficiary === "11",
          centersForDisabilities: init.type === "01",
          includeOwnerQulfic: init.centerOwner_r.ownerType === OWNER_TYPE.NATURAL_TYPE && (init.type === "05" || init.type === "03"),
          ...init,
        }
        setCenterLicenseNumber(init.centerLicense_r.LicenseNumber)
        return init
      })
    }
    if (centerLicenseNumber && formType === LICENSE_FORM_TYPES.EDIT) {
      const response = await getTaskDetails()
      // editInitValues.center[0]
      // setCenter(editInitValues.center[0])
    }
    if (requestNum && fromDraft) {
      console.log(`CreateFinalLicense :: useEffect :: requestNum :: ${requestNum}`)
      const response = await getDraftDetails(requestNum)
      // editInitValues.center[0]
      // setCenter(editInitValues.center[0])
    }
    if (centerLicenseNumber && formType === LICENSE_FORM_TYPES.RENEW && !fromDraft) {
      console.log(`CreateFinalLicense :: useEffect :: LICENSE_FORM_TYPES.RENEW `)
      const response = await getCentertDetails(centerLicenseNumber)
      setEditMode(false)
    }
    let response = await getLookups(2)

    if (!response.isSuccessful) {
      response = { isSuccessful: false, message: response.message };
    } else {
      setStaffTypes(response?.responseBody?.data?.lookup?.["Staff-Types"]?.content)
      console.log(`CreateFinalLincense :: staffTypes :${JSON.stringify(staffTypes)}`)
    }
    console.log(`CreateFinalLincense :: response :${JSON.stringify(response)}`)
    setIsLoading(false);
  }, [])

  const getDraftDetails = async () => {
    setEditMode(true)
    setErrMessage("");
    const response = await DraftDetails(requestNum)
    const requestDetails = response.responseBody.requestDetails
    console.log(`getDraftDetails :: response: + ${JSON.stringify(response)}`)
    console.log(`getDraftDetails :: response.responseBody.requestDetails.data.draft_values: + ${JSON.stringify(requestDetails.data.draft_values)}`)
    console.log(`getDraftDetails :: response.responseBody.requestDetails.data.draft_values.center: + ${JSON.stringify(requestDetails.data.draft_values.center)}`)
    if (!response.isSuccessful)
      setErrMessage(response.message)
    else {
      const draftData = { ...requestDetails.data, staff: requestDetails.data.draft_values.staff }
      setEditInitValues(draftData)
      setDraftValues(requestDetails.data.draft_values.draft_values)
      setCenter(requestDetails.data.draft_values.center)
      // setIsLoading(false)
      return requestDetails.data.draft_values.center
    }
  }

  const getTaskDetails = async () => {
    setEditMode(true)
    setErrMessage("");
    const response = await TaskDetails(taskID)
    console.log("getTaskDetails ===============> response:" + JSON.stringify(response))
    if (!response.isSuccessful)
      setErrMessage(response.message)
    else {
      setEditInitValues(response.responseBody.data)
      setCenter(response.responseBody.data.center[0])
      setIsLoading(false)
      return response.responseBody.data
    }
  }

  const getCentertDetails = async (centerLicenseNumber) => {
    setIsLoading(true)
    setErrMessage("");
    const response = await CentertDetails(centerLicenseNumber)
    console.log("getCentertDetails ===============> response:" + JSON.stringify(response))
    if (!response.isSuccessful) {
      setErrMessage(response.message);
    }
    else {
      const data = response.responseBody.data;
      setEditInitValues({ ...data, inHouseHspit: data.center.type === "08" && data.center.targetedBeneficiary === "11" })
      setCenter(data.center)
      setEditMode(true)
      setIsLoading(false)
      // setShowSummary(true);
      return response.responseBody.data;
    }
  }

  const onSubmit = async (values) => {
    console.log("CreateFinalLicense :: onSubmit")
    console.log('CreateFinalLicense :: editMode: ' + editMode)
    console.log('CreateFinalLicense :: formType ' + formType)
    console.log('CreateFinalLicense :: values.isDraft ' + values.isDraft)
    console.log('CreateFinalLicense :: values: ' + JSON.stringify(values))
    setErrMessage("")
    setIsLoading(true)
    let response = null
    if (!values.isDraft) {
      if (values && values.formType === LICENSE_FORM_TYPES.RENEW) {
        response = await updateFinalLicenseAPIFunc(values, formType, 0, false, requestNum);
        if (response.isSuccessful && !!response?.responseBody?.data) {
          handleClickOpen(`${response.responseBody.data[0]}`, '');
        }
        else {
          setErrMessage(`${response.message}`);
          setIsLoading(false)
        }
      }
      else if (!editMode) {
        response = await updateFinalLicenseAPIFunc(values, formType, 0, false, requestNum);
        if (response.isSuccessful && !!response?.responseBody?.data) {
          handleClickOpen(`${response.responseBody.data.message[0]}`);
        }
        else {
          setErrMessage(`${response.message}`);
          setIsLoading(false)
        }
      }
      else {
        response = await updateFinalLicenseAPIFunc(values, formType, taskID, false, requestNum);
        if (response.isSuccessful && !!response?.responseBody?.data) {
          handleClickOpen(`${response.responseBody.data.message[0]}`);
        }
        else {
          setErrMessage(`${response.message}`);
          setIsLoading(false)
        }
      }
    }
    else {
      // handleClickOpen(` the application is draft and formType is ${values.formType} `, '');
      response = await updateFinalLicenseAPIFunc(values, formType, 0, true, requestNum);
      if (response.isSuccessful && !!response?.responseBody?.data) {
        handleClickOpen(`${response.responseBody.data.message[0]}`);
      }
      else {
        setErrMessage(`${response.message}`);
        setIsLoading(false)
      }
    }
  };

  const handleBackBtn = (values) => {
    delete values.agree;
    delete values.isDraft;
    navigate('/center-services/finallicense', {
      replace: true,
      state: { backValues: values }
    });
  };

  const handleClickOpen = (dialogContent, dialogTitle) => {
    setDialogContent(dialogContent);
    setDialogTitle(dialogTitle)
    setOpen(true);
  };

  const handleClose = (value) => {
    setOpen(false);
    navigate('/app/dashboard', { replace: true });
  };
  return (
    <Container maxWidth="md">
      <Card>
        {!isLoading && formType != LICENSE_FORM_TYPES.RENEW && (
          <CardHeader
            title={editMode && !fromDraft ?
              `تعديل طلب ترخيص نهائي - ${requestNum}`
              :
              title}
          />
        )}
        {!isLoading && formType === LICENSE_FORM_TYPES.RENEW && (
          <CardHeader
            title={`طلب تجديد رخصة نهائية`}
          />
        )}
        <Divider />
        {!isLoading && !fromDraft && editMode &&
          <Alert variant="outlined" severity="warning" sx={{ marginLeft: 2, marginRight: 2, marginTop: 1 }}>
            <AlertTitle> يرجى مراجعة طلب رقم {requestNum}</AlertTitle>
            {editInitValues.request && editInitValues.request?.comment}
          </Alert>
        }
        {!isLoading && fromDraft &&
          <Alert icon={<DraftsTwoToneIcon sx={{ color: 'grey !important' }} />} variant="outlined" severity="info" sx={{ marginLeft: 2, marginRight: 2, marginTop: 1, color: 'grey !important', borderColor: 'grey !important' }}>
            <AlertTitle> مسودة رقم {requestNum}</AlertTitle>
            {editInitValues?.request && editInitValues.request?.comment}
          </Alert>
        }
        {errMessage && (
          <Alert variant="outlined" severity="error">
            {errMessage}
          </Alert>
        )}
        <CardContent>
          {!isLoading ?
            <>
              <FinalFromWizard
                initialValues={
                  !editMode && !fromDraft ? {
                    agree: [false],
                    isNextBtnDisabled: false,
                    managersCount: 0,
                    teachersCount: 0,
                    page: formType === LICENSE_FORM_TYPES.RENEW ? 1 : 0,
                    staffTypesInitialValues: staffTypes,
                    formType: formType,
                    centerWorkingHours: {},
                    centerLicenseNumber: centerLicenseNumber,
                    managerBD: {},
                    lookupValues: lookupValues,
                    requestNum: requestNum || initValues.requestNum,
                    ...initValues
                  } : {
                    agree: [false],
                    isNextBtnDisabled: false,
                    managersCount: 0,
                    teachersCount: 0,
                    centerType: centerData?.type || center && center.type && center.targetedBeneficiary && center.targetedServices
                      && centerTypeJSON.type[parseInt(center.type)] && centerTypeJSON.targetedBeneficiary[parseInt(center.targetedBeneficiary)] && centerTypeJSON.targetedBeneficiary[parseInt(center.targetedBeneficiary)] && centerTypeJSON.targetedServices[parseInt(center.targetedServices)]
                      && centerTypeJSON.type[parseInt(center.type)].name + ' - ' + centerTypeJSON.targetedBeneficiary[parseInt(center.targetedBeneficiary)].name + ' - ' + centerTypeJSON.targetedServices[parseInt(center.targetedServices)].name,
                    centerLicenseNumber: centerData?.centerLicense_r.LicenseNumber || center && center.centerLicense_r.LicenseNumber,
                    licenseCreationDate: center && dateFormatter(center.creationDate),
                    licenseExpiryDate: center && dateFormatter(center.expirationDate),
                    ownerName: center && center.centerOwner_r?.ownerName,
                    ownerID: center && center.centerOwner_r?.ownerID,
                    centerAgeGroup: center && center.ageGroup && reverseRange(center.ageGroup),
                    centerGenderGroup: center
                      && center.targetedGender &&
                      (center.targetedGender === "m" ? "ذكر" : (center.targetedGender === "f" ? "أنثى" : "كلا الجنسين")),
                    companyName: center && center.crInfo_r && center.crInfo_r.entityName,
                    activities: center && center.crInfo_r && center.crInfo_r.crActivityType,
                    municipLicenseNo: center && center.crInfo_r && center.crInfo_r.MoMRA_License,
                    beneficiariesNum: center && center.centerInfo_r && center.centerInfo_r.beneficiaryCount,
                    capacity: center && center.centerInfo_r && numeral(center.centerInfo_r.carryingnumber).format('0,0'),
                    financialGuarantee: center && center.centerInfo_r && `${numeral(center.centerInfo_r.financialGuarantee).format('0,0.00')} ر.س.`,
                    buildingArea: center && center.centerInfo_r && center.centerInfo_r.buildingArea,
                    basementArea: center && center.centerInfo_r && center.centerInfo_r.basementArea,
                    OperationalPlan: [center && center.centerInfo_r && center.centerInfo_r.operationPlan && (center.centerInfo_r.operationPlan || center.centerInfo_r.operationPlan.id)],
                    ExecutivePlan: [center && center.centerInfo_r && center.centerInfo_r.executivePlan && (center.centerInfo_r.executivePlan || center.centerInfo_r.executivePlan.id)],
                    OfficeReport: [center && center.centerInfo_r && center.centerInfo_r.engineeringPlan && (center.centerInfo_r.engineeringPlan || center.centerInfo_r.engineeringPlan.id)],
                    SecurityReport: center && center.centerInfo_r && [center.centerInfo_r.securityReport && (center.centerInfo_r.securityReport || center.centerInfo_r.securityReport.id)],
                    Furniture: center && center.centerInfo_r && center.centerInfo_r.furniturePhoto_r && (center.centerInfo_r.furniturePhoto_r.map(d => d.Document) || center.centerInfo_r.furniturePhoto_r.map(d => d.Document.id)),
                    // Furniture: [1202],
                    FinancialGuaranteeAtt: [center && center.centerInfo_r && center.centerInfo_r.financialGuarbteeAtt && (center.centerInfo_r.financialGuarbteeAtt || center.centerInfo_r.financialGuarbteeAtt.id)],
                    healthServices: center && center.centerInfo_r && center.isHealthCareServices ? "yes" : "no",
                    healthServiceType: center && center.centerInfo_r && center.healthCareServices_r && center.healthCareServices_r.type,
                    // healthServiceAttachment: center.centerInfo_r.financialGuarbteeAtt,
                    healthServiceAttachment: [center && center.centerInfo_r && center.healthCareServices_r && center.healthCareServices_r.attachment && (center.healthCareServices_r.attachment || center.healthCareServices_r.attachment.id)],
                    customers: editInitValues?.staff && getStaff(editInitValues?.staff),
                    page: formType === LICENSE_FORM_TYPES.RENEW ? 1 : 0,
                    formType: formType,
                    centerLicenseNumber: centerLicenseNumber,
                    managerBD: {},
                    lookupValues: lookupValues,
                    requestNum: requestNum || initValues.requestNum,
                    ...draftValues
                  }}
                isEnableNextBtn={isEnableNextBtn}
                onSubmit={onSubmit}
                firstBackBtnFunc={handleBackBtn}
                cancelBtnFn={() => { navigate('/app/center-services-list', { replace: true }); }}
                isEnableCancelBtn={!!centerLicenseNumber} // formType === LICENSE_FORM_TYPES.TEMP
                isEnableEndBtn={!centerLicenseNumber}
                requestNum={requestNum}
                email={email}
                canShowSection={canShowSection}
              >

                <FinalFromWizardCenterDetailsPage
                  centerLicenseNumber={centerLicenseNumber}
                  setCenterLicenseNumber={(centerLicenseNumber) => setCenterLicenseNumber(centerLicenseNumber)}
                  validate={CenterDetailsValidation}
                  temporaryLicenses={temporaryLicenses}
                  editMode={editMode}
                  setEditMode={setEditMode}
                  setIsEnableNextBtn={(isEnable) => setIsEnableNextBtn(isEnable)}
                  label="معلومات المركز"
                  fromDraft={fromDraft} />

                {!centersForDisabilities &&
                  <FinalFromWizardAddressPage
                    label="عنوان المركز"
                    validate={(values) => NewAddressValidation(values)}
                    setErrMessage={(errMessage) => setErrMessage(errMessage)}
                    setIsEnableNextBtn={(isEnable) =>
                      setIsEnableNextBtn(isEnable)}
                    setField={(fieldName, fieldValue) =>
                      setField(fieldName, fieldValue)}
                  />}

                {centersForDisabilities && <FinalFromWizardCapacityPage
                  validate={capacityValidation}
                  editMode={editMode}
                  setIsEnableNextBtn={(isEnable) => setIsEnableNextBtn(isEnable)}
                  label="الطاقة الإستيعابية والضمان المالي" />}


                <FinalFromWizardRequirements
                  validate={(values) => RequirementsValidation(values)}
                  setIsEnableNextBtn={(isEnable) => setIsEnableNextBtn(isEnable)}
                  setErrMessage={(errMessage) => setErrMessage(errMessage)}
                  label="المتطلبات"
                />

                {centersForDisabilities &&
                  <FinalFromWizardHealthServices
                    validate={(values) => healthServicesValidation(values)}
                    label="الخدمات الصحية"
                    editMode={editMode} />}

                {centersForDisabilities &&
                  <FinalFromWizardPersonsPage
                    nextFun={(values) => personsValidation(values)}
                    label="معلومات الكوادر"
                    editMode={editMode} />}


                {!centersForDisabilities &&
                  <FinalFromWizardCenterManagerInfo
                    label={initValues?.type === "08" && initValues?.targetedBeneficiary === "11" ? "بيانات مالكة المركز (مقر الضيافة المنزلية)" : "بيانات مدير/ة المركز"}
                    editMode={editMode}
                    setIsEnableNextBtn={(isEnable) => setIsEnableNextBtn(isEnable)}
                    validate={(values) => CenterMangerInfoValidation(values)}
                    setErrMessage={(errMessage) => setErrMessage(errMessage)}
                  />}

                < FinalFromWizardSummary
                  centersForDisabilities={centersForDisabilities}
                  label="الملخص"
                />
              </FinalFromWizard>
            </>
            :
            <CircularProgress size="15rem" style={{
              display: 'block',
              marginLeft: 'auto',
              marginRight: 'auto', color: '#E2E8EB'
            }} />
          }
        </CardContent>
      </Card>
      <AlertDialog dialogContent={dialogContent} dialogTitle={dialogTitle} open={open} onClose={handleClose} acceptBtnName="تم" />
    </Container>
  );
};

const FinalFromWizardCenterDetailsPage = ({
  setField,
  temporaryLicenses,
  editMode,
  setEditMode,
  values,
  centerLicenseNumber,
  setCenterLicenseNumber,
  setIsEnableNextBtn,
  fromDraft }) => (
  <>
    <CenterDetails
      Condition={calculationConditionComp}
      values={values}
      centerLicenseNumber={centerLicenseNumber}
      setCenterLicenseNumber={(centerLicenseNumber) => setCenterLicenseNumber(centerLicenseNumber)}
      temporaryLicenses={temporaryLicenses}
      setField={(fieldName, fieldValue) => setField(fieldName, fieldValue)}
      editMode={editMode}
      setIsEnableNextBtn={(isEnable) => setIsEnableNextBtn(isEnable)}
      setEditMode={setEditMode}
      fromDraft={fromDraft}
    />
  </>
);
const FinalFromWizardAddressPage = ({
  setField,
  values,
  setIsEnableNextBtn,
  setErrMessage
}) => (
  <Box>
    <CenterAddress
      Condition={ConditionComp}
      setField={(fieldName, fieldValue) => setField(fieldName, fieldValue)}
      setIsEnableNextBtn={(isEnable) => setIsEnableNextBtn(isEnable)}
      setErrMessage={(errMessage) => setErrMessage(errMessage)}
      values={values}
    />
  </Box>
);

const FinalFromWizardCapacityPage = ({ editMode, values, setField, setIsEnableNextBtn }) => (
  <>
    <Capacity
      Condition={calculationConditionComp}
      values={values}
      setField={(fieldName, fieldValue) => setField(fieldName, fieldValue)}
      setIsEnableNextBtn={(isEnable) => setIsEnableNextBtn(isEnable)}
      editMode={editMode}
    />
  </>
);

const FinalFromWizardRequirements = ({ setField, setErrMessage, temporaryLicenses, values, setIsEnableNextBtn }) => (
  <>
    <Requirements
      Condition={ConditionComp}
      values={values}
      setErrMessage={(errMessage) => setErrMessage(errMessage)}
      temporaryLicenses={temporaryLicenses}
      setField={(fieldName, fieldValue) => setField(fieldName, fieldValue)}
      setIsEnableNextBtn={(isEnable) => setIsEnableNextBtn(isEnable)}
    />
  </>
)

const FinalFromWizardHealthServices = ({ editMode, setField, temporaryLicenses, values }) => (
  <>
    <HealthServices
      Condition={ConditionComp}
      values={values}
      temporaryLicenses={temporaryLicenses}
      setField={(fieldName, fieldValue) => setField(fieldName, fieldValue)}
      editMode={editMode}
    />
  </>
);


const FinalFromWizardCenterManagerInfo = ({ editMode, setField, values, setIsEnableNextBtn, setErrMessage }) => (
  <>
    <CenterManagerInfo
      Condition={calculationConditionComp}
      setIsEnableNextBtn={(isEnable) => setIsEnableNextBtn(isEnable)}
      setErrMessage={(errMessage) => setErrMessage(errMessage)}
      setField={(fieldName, fieldValue) => setField(fieldName, fieldValue)}
      values={values}
      editMode={editMode}
    />
  </>
);
const FinalFromWizardPersonsPage = ({ editMode, setField, pop, push, values }) => (
  <>
    <PersonDetials
      MedicalPracticeCondition={MedicalPracticeComp}
      Condition={ConditionComp}
      setField={(fieldName, fieldValue) => setField(fieldName, fieldValue)}
      pop={pop}
      push={push}
      values={values}
      editMode={editMode}
    />
  </>
);

const FinalFromWizardSummary = ({ setField, temporaryLicenses, values, centersForDisabilities }) => (
  <>
    {values.formType != LICENSE_FORM_TYPES.RENEW ?
      <Summary
        values={values}
        centersForDisabilities={centersForDisabilities}
        temporaryLicenses={temporaryLicenses}
        setField={(fieldName, fieldValue) => setField(fieldName, fieldValue)}
      />
      :
      <RenewalSummary
        values={values}
        setField={(fieldName, fieldValue) => setField(fieldName, fieldValue)}
      />
    }
  </>
);

export default CreateFinalLicense;
