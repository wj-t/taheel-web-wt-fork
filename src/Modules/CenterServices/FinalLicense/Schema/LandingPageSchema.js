import { v4 as uuid } from 'uuid';

export default [
  {
    id: uuid(),
    label: {
      ar: "تاريخ الطلب",
      en: 'centerCreationDate'
    },
    name: 'creationHijri',
    type: 'Text',
    gridSize: '6',
  },
  {
    id: uuid(),
    label: {
      ar: "فئة المركز",
      en: 'center Type'
    },
    name: 'centerType',
    type: 'Select',
    gridSize: '6',
  },
  {
    id: uuid(),
    label: {
      ar: "نوع المركز",
      en: 'targeted Benificiray'
    },
    name: 'targetedBenificiray',
    type: 'Select',
    gridSize: '6',
  },
  {
    id: uuid(),
    label: {
      ar: "اختصاص المركز",
      en: 'targetedServices'
    },
    name: 'targetedServices',
    type: 'Select',
    gridSize: '6',
  },
  {
    id: uuid(),
    label: {
      ar: "جنس المستفيد",
      en: 'targetedGender'
    },
    name: 'targetedGender',
    type: 'Select',
    gridSize: '6',
    options: [
      { value: 'm', label: { ar: 'ذكور' } },
      { value: 'f', label: { ar: 'إناث' } },
      { value: 'b', label: { ar: 'كلا الجنسين' } }
    ],
  },
  {
    id: uuid(),
    label: {
      ar: "فترة العمل",
      en: 'workingHours'
    },
    name: 'workingHours',
    type: 'Radio',
    options: [
      { value: 'morning', label: { ar: 'القترة الصباحية' } },
      { value: 'evening', label: { ar: 'الفترة المسائية' } },
      { value: 'both', label: { ar: 'فترتين' } },
      {
        value: '6-12',
        label: { ar: 'السادسة صباحاً حتى العاشرة مساءً' },
        forEldery: true
      },
      {
        value: 'allDay',
        label: { ar: 'طوال أيام الأسبوع' },
        forEldery: true
      }
    ],
    gridSize: '6',
  },

];
