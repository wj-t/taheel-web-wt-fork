/* eslint-disable no-unused-vars */
import { Alert, Grid, Typography } from '@material-ui/core';
import { Box } from '@mui/system';
import moment from 'moment-hijri';
import PropTypes from 'prop-types';
import { useEffect, useState } from 'react';
import { Field } from 'react-final-form';
import Calendar from 'src/Core/Components/calendar';
import FileUploaderComp from '../Components/FileUploader';

const Requirements = ({ setField, values, setIsEnableNextBtn, setErrMessage }) => {
  const [centersForDisabilities, setCentersForDisabilities] = useState(values.type === "01");
  var multipleFileDocs = [];
  useEffect(() => { setErrMessage('') }, [])

  const setDocument = (name, docID, multipleFile) => {
    if (!multipleFile) setField(name, [docID]);
    else {
      multipleFileDocs.push(docID);
      setField(name, multipleFileDocs);
    }
  };
  setIsEnableNextBtn(true);
  return (
    <>
      {centersForDisabilities &&
        <Grid container spacing={3} mt={3}>
          <Grid item md={12} xs={12}></Grid>
          <Grid item md={6} xs={12}>
            <Field
              label="ارفاق الخطة التشغيلية"
              name="OperationalPlan"
              component={FileUploaderComp}
              multipleFile={false}
              setField={setField}
              setDocument={setDocument}
              values={values}
            />
          </Grid>
          <Grid item md={6} xs={12}>
            <Field
              label="ارفاق الخطة التنفيذية"
              name="ExecutivePlan"
              component={FileUploaderComp}
              multipleFile={false}
              setField={setField}
              setDocument={setDocument}
              values={values}
            />
          </Grid>
          <Grid item md={6} xs={12}>
            <Field
              label="ارفاق تقرير زيارة مكتب هندسي معتمد"
              name="OfficeReport"
              component={FileUploaderComp}
              multipleFile={false}
              setField={setField}
              setDocument={setDocument}
              values={values}
            />
          </Grid>
          <Grid item md={6} xs={12}>
            <Field
              label="ارفاق تقرير المسح الأمني"
              name="SecurityReport"
              component={FileUploaderComp}
              multipleFile={false}
              setField={setField}
              setDocument={setDocument}
              values={values}
            />
          </Grid>
          <Grid item md={6} xs={12}>
            <Field
              label="ارفاق صور الأثاث و الأجهزة الكهربائية"
              name="Furniture"
              component={FileUploaderComp}
              setField={setField}
              setDocument={setDocument}
              values={values}
              multipleFile={true}
            />
          </Grid>
          <Grid item md={6} xs={12}>
            <Field
              label="ارفاق الضمان المالي"
              name="FinancialGuaranteeAtt"
              component={FileUploaderComp}
              multipleFile={false}
              setField={setField}
              setDocument={setDocument}
              values={values}
            />
          </Grid>
        </Grid>}
      {!centersForDisabilities && <>
        <Grid container spacing={3} mt={3}>
          <Grid item md={12} xs={12}></Grid>
          {values.includeOwnerQulfic && (
            <Grid item md={6} xs={12}>
              <Field
                label="المؤهل التعليمي لمالك المركز"
                name="ownerEducationalQualifications"
                component={FileUploaderComp}
                multipleFile={false}
                setField={setField}
                setDocument={setDocument}
                values={values}
              />
            </Grid>)
          }
          <Grid item md={6} xs={12}>

            <Field
              label="إرفاق تقرير زيارة مكتب هندسي معتمد"
              name="OfficeReport"
              component={FileUploaderComp}
              multipleFile={false}
              setField={setField}
              setDocument={setDocument}
              values={values}
            />
          </Grid>
          <Grid item md={6} xs={12}>
            <Field
              label="إرفاق رخصة الدفاع المدني"
              name="fireDepartmentLicense"
              component={FileUploaderComp}
              multipleFile={false}
              setField={setField}
              setDocument={setDocument}
              values={values}
            />
          </Grid>
        </Grid>
        <Grid container spacing={3} mt={2} mb={3}>
          <Grid item md={12} xs={12}>
            <Typography>تاريخ إنتهاء رخصة الدفاع المدني</Typography>
          </Grid>
          <Calendar
            FeiledWidth={4}
            fieldName={"fireDepartmentLicenseExpiryDate"}
            yearCalender={{
              start: moment().format('iYYYY'),
              end: Number.parseInt(moment().format('iYYYY')) + 15
            }}
          />
        </Grid></>}
    </>
  );
};

export default Requirements;
Requirements.propTypes = {
  setField: PropTypes.func.isRequired,
  values: PropTypes.object.isRequired,
  setIsEnableNextBtn: PropTypes.func.isRequired,
  setErrMessage: PropTypes.func.isRequired,
};
