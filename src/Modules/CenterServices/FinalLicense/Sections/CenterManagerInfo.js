import { Alert, Button, CircularProgress, Grid, Typography } from "@material-ui/core"
import FileUploaderComp from '../Components/FileUploader';
import { TextField as TextFieldFinal, Select } from 'final-form-material-ui';
import PropTypes from 'prop-types';
import { Field } from "react-final-form";
import { useEffect, useState } from "react";
import Calendar from 'src/Core/Components/calendar';
import { Box } from "@mui/system";
import { validateCitizenFunc } from "../API/finalLicenseAPI";
import { OnChange } from "react-final-form-listeners";


const CenterManagerInfo = ({ setField, values, Condition, setIsEnableNextBtn, setErrMessage }) => {
    const [loading, setLoading] = useState(false);
    const [checkData, setCheckData] = useState(false);
    const [checkValied, setCheckValied] = useState(false);
    // const [idTitle, setIdTitle] = useState("");

    useEffect(() => {
        // if (values.type === '08') {
        //     setIdTitle("الهوية الوطنية لمديرة المركز")
        // }
        // else {
        //     setIdTitle("الهوية الوطنية  لمدير/ة المركز المرشح/ة")
        // }
        if (values.type === "08" && values.targetedBeneficiary === "11")
            setIsEnableNextBtn(true);
        else {
            setIsEnableNextBtn(false);
        }
        if (values.inHouseHspit) {
            setField("idNumber", values.idNumber)
        }
        if (values.fullName && values.idNumber && values.IDNo && values.managerBD?.year && values.managerBD?.month && values.managerBD?.day) {
            setCheckData(true)
            setLoading(false)
            setIsEnableNextBtn(true)
        } else if (!values.inHouseHspit) {
            setCheckData(false)
            setIsEnableNextBtn(false)
        }
    }, [])

    function numberToDay(day) {
        return ('0' + day).slice(-2);
    }
    function numberToBirthDate(birthOfDate) {
        var year = Number(birthOfDate.substr(0, 4));
        var month = Number(birthOfDate.substr(4, 2));
        var day = Number(birthOfDate.substr(6, 2));
        return day + '/' + month + '/' + year
    }


    const checkManagerInfo = async () => {
        setLoading(true);
        setErrMessage('');

        if (!values.IDNo) {
            setLoading(false);
            setErrMessage('يرجى إدخال رقم الهوية');
            return
        }
        if (values.IDNo.length != 10) {
            setErrMessage("تشير سجلاتنا أن رقم الهوية/ الإقامة المُدخَل غير صحيح. الرجاء التأكد من صحة الرقم.");
            setLoading(false);
            return;
        }
        if (!values.IDNo.startsWith('1')) {
            setErrMessage('تشير سجلاتنا أن صاحب الهوية غير سعودي/سعودية الجنسية');
            setLoading(false)
            return;
        }
        if (!values.managerBD?.year || !values.managerBD?.month || !values.managerBD?.day) {
            setLoading(false)
            setErrMessage('يرجى إدخال تاريخ الميلاد');
            return
        }
        const managerBD = values.managerBD.year + '' + values.managerBD.month + values.managerBD.day;
        console.log("BD", managerBD)

        const getCenterManagerInfo = await validateCitizenFunc(values.IDNo, managerBD, false);
        if (!getCenterManagerInfo.isSuccessful) {
            setErrMessage(getCenterManagerInfo.message)
            setLoading(false)
            setCheckData(false)
            return;

        } else {
            const { firstName, secondName, thirdName, fourthName } = getCenterManagerInfo.responseBody.data.name;
            const birthOfDate = getCenterManagerInfo.responseBody.data.birthDate;
            const BOD = numberToBirthDate(birthOfDate)
            const Gender = getCenterManagerInfo.responseBody.data.gender === "f" ? "أنثى" : "ذكر"
            if (values.type === '08' && Gender != "أنثى") {
                setErrMessage("تشير سجلاتنا أن صاحب الهوية ليست امرأة")
                setLoading(false)
                return;
            }

            setField('fullName', firstName + ' ' + secondName + ' ' + thirdName + ' ' + fourthName)
            setField('idNumber', getCenterManagerInfo.responseBody.data.idNumber)
            setField("birthDate", BOD)
            setField("gender", Gender)
            setCheckData(true)
            setLoading(false)
            setIsEnableNextBtn(true)
            return;
        }

    };

    var multipleFileDocs = [];
    const setDocument = (name, docID, multipleFile) => {
        if (!multipleFile) setField(name, [docID]);
        else {
            multipleFileDocs.push(docID);
            setField(name, multipleFileDocs);
        }
    };
    const handleOnChange = () => {
        setField('fullName', null)
        setField('idNumber', null)
        setField("birthDate", null)
        setField("gender", null)
        setField("CV", null)
        setField("EducationalQualifications", null)
        setCheckData(false)
        setLoading(false)
        setIsEnableNextBtn(false)
    }
    return (
        <>
            <Grid container spacing={3} mt={3}>
                <Grid
                    item
                    md={12}
                    xs={12}
                    className="custom-label-field"
                >
                    <Typography variant="h4" component="div">بيانات مدير/ة المركز</Typography>
                </Grid>
                {values.inHouseHspit ? <> <Grid container md={12} xs={12} spacing={3}>
                <Grid item md={12} xs={12}></Grid>
                    <Grid item md={6} xs={12}>
                        <Field
                            fullWidth
                            required
                            label="رقم الهوية الوطنية"
                            name="idNumber"
                            component={TextFieldFinal}
                            type="number"
                            variant="outlined"
                            dir="rtl"
                            className="custom-field"
                            disabled
                        />
                    </Grid>
                    <Grid item md={6} xs={12}>
                        <Field
                            fullWidth
                            required
                            label="تاريخ الميلاد"
                            name="birthDate"
                            component={TextFieldFinal}
                            type="text"
                            variant="outlined"
                            dir="rtl"
                            className="custom-field"
                            disabled
                        />
                    </Grid>
                    <Grid item md={6} xs={12}>
                        <Field
                            fullWidth
                            required
                            label="الاسم"
                            name="fullName"
                            component={TextFieldFinal}
                            type="text"
                            variant="outlined"
                            dir="rtl"
                            className="custom-field"
                            disabled
                        />
                    </Grid>
                    <Grid item md={6} xs={12}>
                        <Field
                            fullWidth
                            required
                            label="الجنس"
                            name="gender"
                            component={TextFieldFinal}
                            type="text"
                            variant="outlined"
                            dir="rtl"
                            className="custom-field"
                            disabled
                        />
                    </Grid>
                </Grid>
                    <Grid item md={12} xs={12}></Grid>
                    <Grid container md={12} xs={12} spacing={3}>
                        <Grid item md={6} xs={12}>
                            <Field
                                label="إرفاق السيرة الذاتية"
                                name="CV"
                                component={FileUploaderComp}
                                multipleFile={false}
                                setField={setField}
                                setDocument={setDocument}
                                values={values}
                                InputLabelProps={{ shrink: true }}
                            />
                        </Grid>
                        <Grid item md={6} xs={12}>
                            <Field
                                label=" إرفاق المؤهل التعليمي"
                                name="EducationalQualifications"
                                component={FileUploaderComp}
                                multipleFile={false}
                                setField={setField}
                                setDocument={setDocument}
                                values={values}
                            />
                        </Grid>
                        <Grid item md={6} xs={12}>
                            <Field
                                label="إرفاق تقرير طبي"
                                name="MedicalReport"
                                component={FileUploaderComp}
                                multipleFile={false}
                                setField={setField}
                                setDocument={setDocument}
                                values={values}
                            />
                        </Grid>
                        <Grid item md={6} xs={12}>
                            <Field
                                label="إرفاق شهادة إتمام دورة إلاسعافات ألاولية"
                                name="FirstAidCourseCompletionCertificate"
                                component={FileUploaderComp}
                                multipleFile={false}
                                setField={setField}
                                setDocument={setDocument}
                                values={values}
                            />
                        </Grid>
                        <Grid item md={6} xs={12}>
                            <Field
                                label="إرفاق صك ملكية المنزل /عقد إلايجار"
                                name="TitleDeedOrLeaseContract"
                                component={FileUploaderComp}
                                setField={setField}
                                setDocument={setDocument}
                                values={values}
                                multipleFile={false}
                            />
                        </Grid>
                        <Grid item md={6} xs={12}>
                            <Field
                                label="إرفاق مخطط البناء"
                                name="BuildingScheme"
                                component={FileUploaderComp}
                                multipleFile={false}
                                setField={setField}
                                setDocument={setDocument}
                                values={values}
                            />
                        </Grid>
                    </Grid></>
                    :
                    <>
                        <Grid container md={12} xs={12} spacing={3}>
                            <Grid item md={12} xs={12}></Grid>
                            <Grid item md={6} xs={12}>
                                <Field
                                    fullWidth
                                    required
                                    label="رقم الهوية الوطنية"
                                    name="IDNo"
                                    component={TextFieldFinal}
                                    type="text"
                                    variant="outlined"
                                    disabled={loading}
                                    dir="rtl"
                                    className="custom-field"
                                />
                                <OnChange name="IDNo">
                                    {(value, previous) => {
                                        console.log("valuesvalues ", values)
                                        values.IDNo = value?.replace(/\D/g, '')
                                        handleOnChange();
                                        setErrMessage("")

                                    }}
                                </OnChange>
                            </Grid>
                            <Grid item md={12} xs={12} mt={3}>
                                <Typography> تاريخ الميلاد</Typography>
                            </Grid>
                            <Calendar disabled={loading} FeiledWidth={2} fieldName="managerBD" />
                            <OnChange name="managerBD.day">
                                {(value, previous) => {
                                    values.managerBD.day = value
                                    handleOnChange();
                                }}
                            </OnChange>
                            <OnChange name="managerBD.month">
                                {(value, previous) => {
                                    values.managerBD.month = value
                                    handleOnChange();
                                }}
                            </OnChange>
                            <OnChange name="managerBD.year">
                                {(value, previous) => {
                                    values.managerBD.year = value
                                    handleOnChange();
                                }}
                            </OnChange>
                            <Grid item md={6} xs={12}>
                                <Button
                                    startIcon={loading ? <CircularProgress size="1rem" /> : null}
                                    variant='outlined'
                                    type="button"
                                    disabled={loading}
                                    sx={{
                                        height: 55,
                                        backgroundColor: 'white',
                                        width: '100%',
                                        color: '#3c8084',
                                        ':hover': {
                                            backgroundColor: '#3c8084',
                                            color: 'white',
                                        }
                                    }}
                                    onClick={checkManagerInfo}
                                >
                                    تحقق
                                </Button>
                            </Grid>
                        </Grid>
                        <Grid item md={12} xs={12}></Grid>
                        {checkData && <> <Grid container md={12} xs={12} spacing={3}>
                            <Grid item md={6} xs={12}>
                                <Field
                                    fullWidth
                                    required
                                    label="الاسم"
                                    name="fullName"
                                    component={TextFieldFinal}
                                    type="text"
                                    variant="outlined"
                                    dir="rtl"
                                    className="custom-field"
                                    disabled
                                />
                            </Grid>
                            <Grid item md={6} xs={12}>
                                <Field
                                    fullWidth
                                    required
                                    label="الجنس"
                                    name="gender"
                                    component={TextFieldFinal}
                                    type="text"
                                    variant="outlined"
                                    dir="rtl"
                                    className="custom-field"
                                    disabled
                                />
                            </Grid>
                        </Grid>
                            <Grid item md={12} xs={12}></Grid>
                            <Grid container md={12} xs={12} spacing={3}>
                                <Grid item md={6} xs={12}>
                                    <Field
                                        label="إرفاق السيرة الذاتية"
                                        name="CV"
                                        component={FileUploaderComp}
                                        multipleFile={false}
                                        setField={setField}
                                        setDocument={setDocument}
                                        values={values}
                                    />
                                </Grid>
                                <Grid item md={6} xs={12}>
                                    <Field
                                        label=" إرفاق المؤهل التعليمي"
                                        name="EducationalQualifications"
                                        component={FileUploaderComp}
                                        multipleFile={false}
                                        setField={setField}
                                        setDocument={setDocument}
                                        values={values}
                                    />
                                </Grid>



                            </Grid>

                        </>}
                    </>}
            </Grid>
        </>
    )

}
export default CenterManagerInfo
CenterManagerInfo.propTypes = {
    setField: PropTypes.func.isRequired,
    values: PropTypes.object.isRequired,
    Condition: PropTypes.func.isRequired,
    setIsEnableNextBtn: PropTypes.func.isRequired,
    setErrMessage: PropTypes.func.isRequired,
};