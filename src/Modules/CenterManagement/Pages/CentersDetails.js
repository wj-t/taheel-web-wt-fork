/* eslint-disable */
import { useState, useEffect } from 'react'
import { useLocation } from 'react-router-dom'
import { useNavigate } from 'react-router';
import {
    Grid,
    Button,
} from '@material-ui/core';
import { CentertDetails } from 'src/Modules/CenterServices/API/ServicesApi';
import CenterDetailsSchema from '../Schema/CenterDetailsSchema'
import TempLicenseSchema from '../Schema/TempLicenseSchema'
import FormCreator from 'src/Core/SchemaBuilder/FormCreator'
import { getRequestDetails } from 'src/Modules/CenterServices/API/ServicesApi'
import { REQUEST_TYPES, LICENSE_FORM_TYPES, OWNER_TYPE } from 'src/Core/Utils/enums'

import IconsTypeEnum from 'src/Core/SchemaBuilder/Utils/IconsTypeEnum'
import IconsList from 'src/Core/SchemaBuilder/FieldsInputs/IconsList'
import { useLookup, useUpdateLookup } from 'src/Core/Contexts/useLookup';
import { getCurrentUser } from 'src/Core/Utils/UserLocalStorage';
import finalLicenseNewTempsSchema from 'src/Modules/CenterServices/FinalLicense/Schema/finalLicenseNewTempsSchema';
import { formateGetRequestDetails } from 'src/Modules/CenterServices/TransferCenterOwnership/Utils/FormateJson';


const CentersDetails = () => {
    const navigate = useNavigate()
    const lookupValues = useLookup()
    const location = useLocation()
    const refreshLookup = useUpdateLookup()
    const licenseNumber = location.state?.licenseNumber
    const requestNum = location.state?.requestNum
    const requestDetails = location.state?.requestDetails
    console.log("licenceNumber+_+_+_+_+_+_+", licenseNumber)
    console.log("lookupValues ====> ", lookupValues)
    const { DOB } = getCurrentUser();

    const [oldView, setOldView] = useState(true)
    const [taskID, setTaskID] = useState()
    const [alertComment, setAlertComment] = useState()
    const [details, setDetails] = useState(false)
    const [navInfo, setNavInfo] = useState('')
    const [errMessage, setErrMessage] = useState('')
    const [loading, setLoading] = useState(true)
    const [isTemp, setIsTemp] = useState(false)
    useEffect(async () => {
        lookupValues?.isEmpity && (refreshLookup())
        setLoading(true)
        if (!!requestNum) {
            const getReqDetails = await getRequestDetails(requestNum)
            if (getReqDetails.isSuccessful) {
                let reqDetails = getReqDetails.responseBody.requestDetails.data
                setDetails(details => {
                    details = {
                        ...reqDetails.center?.centerInfo_r,
                        ...reqDetails.center,
                        staff: reqDetails.staff,
                        targetedBenificiray: reqDetails.center?.targetedBeneficiary,
                        targetedServices: reqDetails.center?.targetedServices,
                        centerType: reqDetails.center?.type
                    }
                    const centerOwner = reqDetails.center.centerOwner_r
                    if (centerOwner.ownerType === OWNER_TYPE.LEGAL_TYPE) {
                        details.commissionerMobNum = centerOwner.ownerPhoneNumber
                        details.entityName = centerOwner.ownerName
                        details.CRNumber = centerOwner.ownerID
                        details.DOB = null
                        delete details.centerOwner_r.ownerName
                        delete details.centerOwner_r.ownerID
                        delete details.centerOwner_r.ownerPhoneNumber
                    }
                    if (centerOwner.ownerType === OWNER_TYPE.NATURAL_TYPE) {
                        details.DOB = DOB
                    }
                    if (requestDetails?.typeId === REQUEST_TYPES.TEMPOR) {
                        setIsTemp(true)
                    }
                    if (details.centerType != "01" && requestDetails?.typeId != REQUEST_TYPES.TEMPOR) {
                        setOldView(false)
                        details = {
                            ...details,
                            ...formateGetRequestDetails({ ...reqDetails, status: requestDetails.status }),
                        }
                    }

                    return details
                })

                setAlertComment({ msg: reqDetails.request?.comment, title: 'الملاحظات' })
                setTaskID(reqDetails?.externalTaskData?.ID)
                console.log("reqDetails+++++++++++++", reqDetails)
                setLoading(false)
                if (reqDetails.externalTaskData?.type === REQUEST_TYPES.FINAL) {
                    setNavInfo({ url: '/center-services/editfinallicense', btnName: 'تعديل طلب ترخيص نهائي' })
                } else if (reqDetails.externalTaskData?.type === REQUEST_TYPES.RENEW) {
                    setNavInfo({ url: '/center-services/editfinallicense', btnName: 'تعديل طلب تجديد رخصة' })
                } else {
                    setTaskID('')
                }

            }
        } else {
            const getCenterDetails = await CentertDetails(licenseNumber)
            if (!getCenterDetails.isSuccessful) {
                setErrMessage(getCenterDetails.message)
            } else {
                const Details = getCenterDetails.responseBody.data
                if (Details.center?.centerLicense_r?.LicenseType?.trim() === "1") {
                    setIsTemp(true)
                }
                const centerOwner = Details.center.centerOwner_r
                setDetails(details => {
                    details = {
                        ...Details.center.centerInfo_r,
                        ...Details.center,
                        staff: Details.staff,
                        targetedBenificiray: Details.center?.targetedBeneficiary,
                        targetedServices: Details.center?.targetedServices,
                        centerType: Details.center?.type,
                    }
                    if (centerOwner.ownerType === OWNER_TYPE.LEGAL_TYPE) {
                        details.commissionerMobNum = centerOwner.ownerPhoneNumber
                        details.entityName = centerOwner.ownerName
                        details.CRNumber = centerOwner.ownerID
                        details.DOB = null
                        delete details.centerOwner_r.ownerName
                        delete details.centerOwner_r.ownerID
                        delete details.centerOwner_r.ownerPhoneNumber
                    }
                    if (centerOwner.ownerType === OWNER_TYPE.NATURAL_TYPE) {
                        details.DOB = DOB
                    }
                    return details
                })
                console.log("Details+++++++++++++", Details)
            }
        }
        setLoading(false)
    }, [])
    const title = 'تفاصيل المركز'
    const additionalFields = () => {
        return !!taskID &&
            (
                <Grid container
                    p={5}
                    direction="row"
                    justifyContent="flex-start"
                    alignItems="center">
                    <Grid item>
                        <Button
                            variant="contained"
                            color="primary"
                            sx={{
                                backgroundColor: '#3c8084',
                            }}
                            onClick={() => {
                                navigate(navInfo.url, {
                                    state: {
                                        licenseNumber: licenseNumber,
                                        centerLicenseNumber: licenseNumber,
                                        taskID,
                                        requestNum,
                                        centerData: details,
                                        formType: LICENSE_FORM_TYPES.EDIT
                                    }
                                })
                            }}
                        >
                            <IconsList iconType={IconsTypeEnum.EDIT_ICON} label={navInfo.btnName} color="info" />
                        </Button>
                    </Grid>
                </Grid >
            )
    }
    return (
        <FormCreator
            title={title}
            lookupObject={lookupValues}
            schema={isTemp ? TempLicenseSchema : oldView ? CenterDetailsSchema : finalLicenseNewTempsSchema}
            errMessage={errMessage}
            initValues={details}
            additionalFields={additionalFields()}
            alertComment={alertComment}
            isLoading={loading}
            navBackUrl={{ url: !!requestNum ? '/app/center-requests' : '/app/centers', state: { licenseNumber: licenseNumber } }}
            formType='view'
        />
    )
}
CentersDetails.propTypes = {
    // centers: PropTypes.array.isRequired
}

export default CentersDetails